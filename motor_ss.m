Rs = 1e-3
Ls = 1.1e-1
Kt = 53;
Jm = 1.1e-3;
Bm = 1.1e-3
Kp = 14;
%x = [id; iq; wm]
A = [-Rs/Ls 0  0;
    0 -Rs/Ls 0;
    0 Kt/Jm -Bm/Jm]
B = [Kp/Ls 0;
    0 Kp/Ls;
    0 0]
%u = [udd; uqq];
