/*
 * motormodell.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "motormodell".
 *
 * Model version              : 1.1
 * Simulink Coder version : 9.3 (R2020a) 18-Nov-2019
 * C source code generated on : Tue Feb 23 11:09:37 2021
 *
 * Target selection: rsim.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Emulation hardware selection:
 *    Differs from embedded hardware (MATLAB Host)
 * Code generation objective: Execution efficiency
 * Validation result: Passed (3), Warning (1), Error (0)
 */

#ifndef RTW_HEADER_motormodell_h_
#define RTW_HEADER_motormodell_h_
#include <string.h>
#include <stddef.h>
#include <math.h>
#ifndef motormodell_COMMON_INCLUDES_
# define motormodell_COMMON_INCLUDES_
#include <stdlib.h>
#include "rtwtypes.h"
#include "simstruc.h"
#include "fixedpoint.h"
#include "rsim.h"
#include "rt_logging.h"
#include "dt_info.h"
#endif                                 /* motormodell_COMMON_INCLUDES_ */

#include "motormodell_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rt_defines.h"
#include "rt_nonfinite.h"
#define MODEL_NAME                     motormodell
#define NSAMPLE_TIMES                  (2)                       /* Number of sample times */
#define NINPUTS                        (4)                       /* Number of model inputs */
#define NOUTPUTS                       (19)                      /* Number of model outputs */
#define NBLOCKIO                       (4)                       /* Number of data output port signals */
#define NUM_ZC_EVENTS                  (0)                       /* Number of zero-crossing events */
#ifndef NCSTATES
# define NCSTATES                      (4)                       /* Number of continuous states */
#elif NCSTATES != 4
# error Invalid specification of NCSTATES defined in compiler command
#endif

#ifndef rtmGetDataMapInfo
# define rtmGetDataMapInfo(rtm)        (NULL)
#endif

#ifndef rtmSetDataMapInfo
# define rtmSetDataMapInfo(rtm, val)
#endif

/* Block signals (default storage) */
typedef struct {
  real_T Int;                          /* '<S4>/Int' */
  real_T Add;                          /* '<S19>/Add' */
  real_T Add_i;                        /* '<S18>/Add' */
  real_T Divide;                       /* '<S4>/Divide' */
} B;

/* Block states (default storage) for system '<Root>' */
typedef struct {
  int_T Integrator_IWORK;              /* '<S19>/Integrator' */
  int_T Integrator_IWORK_n;            /* '<S18>/Integrator' */
} DW;

/* Continuous states (default storage) */
typedef struct {
  real_T Integrator_CSTATE;            /* '<S19>/Integrator' */
  real_T Int1_CSTATE;                  /* '<S4>/Int1' */
  real_T Integrator_CSTATE_o;          /* '<S18>/Integrator' */
  real_T Int_CSTATE;                   /* '<S4>/Int' */
} X;

/* Periodic continuous state vector (global) */
typedef int_T PeriodicIndX[1];
typedef real_T PeriodicRngX[2];

/* State derivatives (default storage) */
typedef struct {
  real_T Integrator_CSTATE;            /* '<S19>/Integrator' */
  real_T Int1_CSTATE;                  /* '<S4>/Int1' */
  real_T Integrator_CSTATE_o;          /* '<S18>/Integrator' */
  real_T Int_CSTATE;                   /* '<S4>/Int' */
} XDot;

/* State disabled  */
typedef struct {
  boolean_T Integrator_CSTATE;         /* '<S19>/Integrator' */
  boolean_T Int1_CSTATE;               /* '<S4>/Int1' */
  boolean_T Integrator_CSTATE_o;       /* '<S18>/Integrator' */
  boolean_T Int_CSTATE;                /* '<S4>/Int' */
} XDis;

/* External inputs (root inport signals with default storage) */
typedef struct {
  real_T LoadTorque;                   /* '<Root>/LoadTorque' */
  real_T PhaseVoltages[3];             /* '<Root>/PhaseVoltages' */
} ExtU;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  real_T info[15];                     /* '<Root>/info' */
  real_T phase_curr[3];                /* '<Root>/phase_curr' */
  real_T mot_speed;                    /* '<Root>/mot_speed' */
} ExtY;

/* External data declarations for dependent source files */
extern const char *RT_MEMORY_ALLOCATION_ERROR;
extern B rtB;                          /* block i/o */
extern X rtX;                          /* states (continuous) */
extern DW rtDW;                        /* states (dwork) */
extern ExtU rtU;                       /* external inputs */
extern ExtY rtY;                       /* external outputs */

/* Simulation Structure */
extern SimStruct *const rtS;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'motormodell'
 * '<S1>'   : 'motormodell/Interior PMSM'
 * '<S2>'   : 'motormodell/Interior PMSM/PMSM Torque Input Continuous'
 * '<S3>'   : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core'
 * '<S4>'   : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/Mechanical and Angle'
 * '<S5>'   : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/Motor Units1'
 * '<S6>'   : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic'
 * '<S7>'   : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/Motor Units1/Power Accounting Bus Creator'
 * '<S8>'   : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/Motor Units1/Power Accounting Bus Creator/PwrNotTrnsfrd Input'
 * '<S9>'   : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/Motor Units1/Power Accounting Bus Creator/PwrStored Input'
 * '<S10>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/Motor Units1/Power Accounting Bus Creator/PwrTrnsfrd Input'
 * '<S11>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/Clarke Transform'
 * '<S12>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/Inverse Clarke Transform'
 * '<S13>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/Inverse Park Transform'
 * '<S14>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/Machine Torque'
 * '<S15>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/PMSM Equivalent Circuit'
 * '<S16>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/Park Transform'
 * '<S17>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/Machine Torque/Subsystem'
 * '<S18>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage'
 * '<S19>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage1'
 * '<S20>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage/Subsystem'
 * '<S21>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage/Subsystem1'
 * '<S22>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage/Subsystem2'
 * '<S23>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage1/Subsystem'
 * '<S24>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage1/Subsystem1'
 * '<S25>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage1/Subsystem2'
 * '<S26>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage1/Subsystem3'
 */

/* user code (bottom of header file) */
extern const int_T gblNumToFiles;
extern const int_T gblNumFrFiles;
extern const int_T gblNumFrWksBlocks;
extern rtInportTUtable *gblInportTUtables;
extern const char *gblInportFileName;
extern const int_T gblNumRootInportBlks;
extern const int_T gblNumModelInputs;
extern const int_T gblInportDataTypeIdx[];
extern const int_T gblInportDims[];
extern const int_T gblInportComplex[];
extern const int_T gblInportInterpoFlag[];
extern const int_T gblInportContinuous[];

#endif                                 /* RTW_HEADER_motormodell_h_ */
