/*
 * motormodell.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "motormodell".
 *
 * Model version              : 1.1
 * Simulink Coder version : 9.3 (R2020a) 18-Nov-2019
 * C source code generated on : Tue Feb 23 11:09:37 2021
 *
 * Target selection: rsim.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Emulation hardware selection:
 *    Differs from embedded hardware (MATLAB Host)
 * Code generation objective: Execution efficiency
 * Validation result: Passed (3), Warning (1), Error (0)
 */

#include <math.h>
#include "motormodell.h"
#include "motormodell_private.h"
#include "motormodell_dt.h"

/* user code (top of parameter file) */
const int_T gblNumToFiles = 0;
const int_T gblNumFrFiles = 0;
const int_T gblNumFrWksBlocks = 0;
const char *gblSlvrJacPatternFileName =
  "motormodell_rsim_rtw//motormodell_Jpattern.mat";

/* Root inports information  */
const int_T gblNumRootInportBlks = 2;
const int_T gblNumModelInputs = 4;
extern rtInportTUtable *gblInportTUtables;
extern const char *gblInportFileName;
const int_T gblInportDataTypeIdx[] = { 0, 0 };

const int_T gblInportDims[] = { 1, 1, 3, 1 } ;

const int_T gblInportComplex[] = { 0, 0 };

const int_T gblInportInterpoFlag[] = { 1, 1 };

const int_T gblInportContinuous[] = { 1, 1 };

#include "simstruc.h"
#include "fixedpoint.h"

/* Block signals (default storage) */
B rtB;

/* Continuous states */
X rtX;

/* Periodic continuous states */
PeriodicIndX rtPeriodicIndX;
PeriodicRngX rtPeriodicRngX;

/* Block states (default storage) */
DW rtDW;

/* External inputs (root inport signals with default storage) */
ExtU rtU;

/* External outputs (root outports fed by signals with default storage) */
ExtY rtY;

/* Parent Simstruct */
static SimStruct model_S;
SimStruct *const rtS = &model_S;

/* System initialize for root system: '<Root>' */
void MdlInitialize(void)
{
  /* InitializeConditions for Integrator: '<S19>/Integrator' incorporates:
   *  Integrator: '<S18>/Integrator'
   */
  if (ssIsFirstInitCond(rtS)) {
    rtX.Integrator_CSTATE = 0.0;
    rtX.Integrator_CSTATE_o = 0.0;
  }

  rtDW.Integrator_IWORK = 1;

  /* End of InitializeConditions for Integrator: '<S19>/Integrator' */

  /* InitializeConditions for Integrator: '<S4>/Int1' */
  rtX.Int1_CSTATE = 0.0;

  /* InitializeConditions for Integrator: '<S18>/Integrator' */
  rtDW.Integrator_IWORK_n = 1;

  /* InitializeConditions for Integrator: '<S4>/Int' */
  rtX.Int_CSTATE = 0.0;

  /* InitializeConditions for root-level periodic continuous states */
  {
    int_T rootPeriodicContStateIndices[1] = { 1 };

    real_T rootPeriodicContStateRanges[2] = { -3.1415926535897931,
      3.1415926535897931 };

    (void) memcpy((void*)rtPeriodicIndX, rootPeriodicContStateIndices,
                  1*sizeof(int_T));
    (void) memcpy((void*)rtPeriodicRngX, rootPeriodicContStateRanges,
                  2*sizeof(real_T));
  }
}

/* Start for root system: '<Root>' */
void MdlStart(void)
{
  MdlInitialize();
}

/* Outputs for root system: '<Root>' */
void MdlOutputs(int_T tid)
{
  real_T rtb_Integrator;
  real_T rtb_Product;
  real_T rtb_Subtract2;
  real_T rtb_Gain2_f;
  real_T rtb_Add1_a;
  real_T rtb_Add_l;
  real_T rtb_Integrator_b;
  real_T rtb_Add1;
  real_T rtb_Add_m;
  real_T rtb_Gain2_l;
  real_T rtb_Gain1_p;
  real_T rtb_Int1;
  real_T rtb_Gain4;

  /* Read data from the mat file of inport block */
  if (gblInportFileName != (NULL)) {
    int_T currTimeIdx;
    int_T i;

    /*
     *  Read in data from mat file for root inport LoadTorque
     */
    if (gblInportTUtables[0].nTimePoints > 0) {
      if (1) {
        {
          real_T time = ssGetTaskTime(rtS,0);
          int k = 1;
          if (gblInportTUtables[0].nTimePoints == 1) {
            k = 0;
          }

          currTimeIdx = rt_getTimeIdx(gblInportTUtables[0].time, time,
            gblInportTUtables[0].nTimePoints,
            gblInportTUtables[0].currTimeIdx,
            1,
            0);
          gblInportTUtables[0].currTimeIdx = currTimeIdx;
          for (i = 0; i < 1; i++) {
            real_T* realPtr1 = (real_T*)gblInportTUtables[0].ur +
              i*gblInportTUtables[0].nTimePoints +currTimeIdx;
            real_T* realPtr2 = realPtr1 + 1*k;
            (void)rt_Interpolate_Datatype(
              realPtr1,
              realPtr2,
              &rtU.LoadTorque,
              time,
              gblInportTUtables[0].time[currTimeIdx],
              gblInportTUtables[0].time[currTimeIdx + k],
              gblInportTUtables[0].uDataType);
          }
        }
      }
    }

    /*
     *  Read in data from mat file for root inport PhaseVoltages
     */
    if (gblInportTUtables[1].nTimePoints > 0) {
      if (1) {
        {
          real_T time = ssGetTaskTime(rtS,0);
          int k = 1;
          if (gblInportTUtables[1].nTimePoints == 1) {
            k = 0;
          }

          currTimeIdx = rt_getTimeIdx(gblInportTUtables[1].time, time,
            gblInportTUtables[1].nTimePoints,
            gblInportTUtables[1].currTimeIdx,
            1,
            0);
          gblInportTUtables[1].currTimeIdx = currTimeIdx;
          for (i = 0; i < 3; i++) {
            real_T* realPtr1 = (real_T*)gblInportTUtables[1].ur +
              i*gblInportTUtables[1].nTimePoints +currTimeIdx;
            real_T* realPtr2 = realPtr1 + 1*k;
            (void)rt_Interpolate_Datatype(
              realPtr1,
              realPtr2,
              &rtU.PhaseVoltages[i],
              time,
              gblInportTUtables[1].time[currTimeIdx],
              gblInportTUtables[1].time[currTimeIdx + k],
              gblInportTUtables[1].uDataType);
          }
        }
      }
    }
  }

  /* end read inport data from file */

  /* Integrator: '<S19>/Integrator' */
  /* Limited  Integrator  */
  if (rtDW.Integrator_IWORK != 0) {
    rtX.Integrator_CSTATE = 0.0;
  }

  if (rtX.Integrator_CSTATE >= 1.0E+7) {
    if (rtX.Integrator_CSTATE != 1.0E+7) {
      rtX.Integrator_CSTATE = 1.0E+7;
      ssSetBlockStateForSolverChangedAtMajorStep(rtS);
    }
  } else {
    if ((rtX.Integrator_CSTATE <= -1.0E+7) && (rtX.Integrator_CSTATE != -1.0E+7))
    {
      rtX.Integrator_CSTATE = -1.0E+7;
      ssSetBlockStateForSolverChangedAtMajorStep(rtS);
    }
  }

  rtb_Integrator = rtX.Integrator_CSTATE;

  /* Product: '<S26>/Product' incorporates:
   *  Constant: '<S26>/Constant'
   *  Integrator: '<S19>/Integrator'
   */
  rtb_Product = rtX.Integrator_CSTATE * 0.02 / 0.0032;

  /* Integrator: '<S4>/Int1' */
  rtb_Int1 = rtX.Int1_CSTATE;

  /* Gain: '<S4>/Gain4' incorporates:
   *  Integrator: '<S4>/Int1'
   */
  rtb_Gain4 = 3.0 * rtX.Int1_CSTATE;

  /* Trigonometry: '<S6>/sine_cosine' */
  rtb_Gain2_f = sin(rtb_Gain4);
  rtb_Gain4 = cos(rtb_Gain4);

  /* Sum: '<S11>/Add' incorporates:
   *  Gain: '<S11>/Gain'
   *  Gain: '<S11>/Gain1'
   *  Gain: '<S11>/Gain4'
   *  Inport: '<Root>/PhaseVoltages'
   */
  rtb_Subtract2 = (0.66666666666666663 * rtU.PhaseVoltages[0] +
                   -0.33333333333333331 * rtU.PhaseVoltages[1]) +
    -0.33333333333333331 * rtU.PhaseVoltages[2];

  /* Sum: '<S11>/Add1' incorporates:
   *  Gain: '<S11>/Gain2'
   *  Gain: '<S11>/Gain3'
   *  Inport: '<Root>/PhaseVoltages'
   */
  rtb_Add1_a = 0.57735026918962573 * rtU.PhaseVoltages[1] + -0.57735026918962573
    * rtU.PhaseVoltages[2];

  /* Sum: '<S16>/Add' incorporates:
   *  Product: '<S16>/Product'
   *  Product: '<S16>/Product1'
   */
  rtb_Add_l = rtb_Add1_a * rtb_Gain4 - rtb_Subtract2 * rtb_Gain2_f;

  /* Integrator: '<S18>/Integrator' */
  /* Limited  Integrator  */
  if (rtDW.Integrator_IWORK_n != 0) {
    rtX.Integrator_CSTATE_o = 0.0;
  }

  if (rtX.Integrator_CSTATE_o >= 1.0E+7) {
    if (rtX.Integrator_CSTATE_o != 1.0E+7) {
      rtX.Integrator_CSTATE_o = 1.0E+7;
      ssSetBlockStateForSolverChangedAtMajorStep(rtS);
    }
  } else {
    if ((rtX.Integrator_CSTATE_o <= -1.0E+7) && (rtX.Integrator_CSTATE_o !=
         -1.0E+7)) {
      rtX.Integrator_CSTATE_o = -1.0E+7;
      ssSetBlockStateForSolverChangedAtMajorStep(rtS);
    }
  }

  rtb_Integrator_b = rtX.Integrator_CSTATE_o;

  /* End of Integrator: '<S18>/Integrator' */

  /* Integrator: '<S4>/Int' */
  /* Limited  Integrator  */
  if (rtX.Int_CSTATE >= 1.0E+7) {
    if (rtX.Int_CSTATE != 1.0E+7) {
      rtX.Int_CSTATE = 1.0E+7;
      ssSetBlockStateForSolverChangedAtMajorStep(rtS);
    }
  } else {
    if ((rtX.Int_CSTATE <= -1.0E+7) && (rtX.Int_CSTATE != -1.0E+7)) {
      rtX.Int_CSTATE = -1.0E+7;
      ssSetBlockStateForSolverChangedAtMajorStep(rtS);
    }
  }

  rtB.Int = rtX.Int_CSTATE;

  /* End of Integrator: '<S4>/Int' */

  /* Gain: '<S4>/Gain' */
  rtb_Add1 = 3.0 * rtB.Int;

  /* Sum: '<S19>/Add' incorporates:
   *  Constant: '<S25>/Constant'
   *  Product: '<S19>/Product'
   *  Product: '<S23>/Product'
   *  Product: '<S24>/Product'
   *  Product: '<S25>/Product'
   */
  rtB.Add = ((rtb_Add_l / 0.0032 - rtb_Integrator_b * rtb_Add1 * 0.0017 / 0.0032)
             - rtb_Add1 * 0.2205 / 0.0032) - rtb_Product;

  /* Product: '<S18>/Product' */
  rtb_Product = rtb_Add1 * rtb_Integrator;

  /* Sum: '<S16>/Add1' incorporates:
   *  Product: '<S16>/Product2'
   *  Product: '<S16>/Product3'
   */
  rtb_Add1 = rtb_Subtract2 * rtb_Gain4 + rtb_Add1_a * rtb_Gain2_f;

  /* Sum: '<S18>/Add' incorporates:
   *  Constant: '<S22>/Constant'
   *  Product: '<S20>/Product'
   *  Product: '<S21>/Product'
   *  Product: '<S22>/Product'
   */
  rtB.Add_i = (rtb_Product * 0.0032 / 0.0017 + rtb_Add1 / 0.0017) -
    rtb_Integrator_b * 0.02 / 0.0017;

  /* Sum: '<S13>/Add1' incorporates:
   *  Product: '<S13>/Product2'
   *  Product: '<S13>/Product3'
   */
  rtb_Add1_a = rtb_Integrator_b * rtb_Gain4 - rtb_Integrator * rtb_Gain2_f;

  /* Sum: '<S13>/Add' incorporates:
   *  Product: '<S13>/Product'
   *  Product: '<S13>/Product1'
   */
  rtb_Subtract2 = rtb_Integrator_b * rtb_Gain2_f + rtb_Integrator * rtb_Gain4;

  /* Sum: '<S12>/Subtract1' incorporates:
   *  Gain: '<S12>/Gain2'
   *  Gain: '<S12>/Gain3'
   */
  rtb_Gain4 = -0.5 * rtb_Add1_a + 0.8660254037844386 * rtb_Subtract2;

  /* Sum: '<S12>/Subtract2' incorporates:
   *  Gain: '<S12>/Gain1'
   *  Gain: '<S12>/Gain4'
   */
  rtb_Subtract2 = -0.5 * rtb_Add1_a + -0.8660254037844386 * rtb_Subtract2;

  /* Gain: '<S14>/Gain2' incorporates:
   *  Gain: '<S14>/Gain1'
   *  Product: '<S14>/Product'
   *  Product: '<S17>/Product'
   *  Sum: '<S14>/Add'
   */
  rtb_Gain2_f = (rtb_Integrator_b * rtb_Integrator * -0.0015000000000000002 +
                 0.2205 * rtb_Integrator) * 4.5;

  /* Gain: '<S5>/Gain' incorporates:
   *  Product: '<S5>/Product3'
   */
  rtb_Product = -(rtB.Int * rtb_Gain2_f);

  /* Sum: '<S5>/Add' incorporates:
   *  Inport: '<Root>/PhaseVoltages'
   *  Product: '<S5>/Product'
   *  Product: '<S5>/Product1'
   *  Product: '<S5>/Product2'
   */
  rtb_Add_m = (rtb_Add1_a * rtU.PhaseVoltages[0] + rtb_Gain4 *
               rtU.PhaseVoltages[1]) + rtb_Subtract2 * rtU.PhaseVoltages[2];

  /* Gain: '<S5>/Gain1' incorporates:
   *  Constant: '<S5>/Constant'
   *  Product: '<S5>/Product11'
   *  Product: '<S5>/Product12'
   *  Product: '<S5>/Product4'
   *  Sum: '<S5>/Add2'
   */
  rtb_Gain1_p = -((rtb_Integrator_b * rtb_Integrator_b + rtb_Integrator *
                   rtb_Integrator) * 0.02);

  /* Gain: '<S5>/Gain2' incorporates:
   *  Abs: '<S4>/Abs'
   *  Product: '<S4>/Product3'
   *  Product: '<S4>/Product4'
   *  Sum: '<S4>/Add'
   */
  rtb_Gain2_l = -(rtB.Int * rtB.Int * 0.0004924 + fabs(rtB.Int) * 0.0);

  /* Outport: '<Root>/info' incorporates:
   *  Sum: '<S5>/Add4'
   */
  rtY.info[0] = rtb_Add1_a;
  rtY.info[1] = rtb_Gain4;
  rtY.info[2] = rtb_Subtract2;
  rtY.info[3] = rtb_Integrator_b;
  rtY.info[4] = rtb_Integrator;
  rtY.info[5] = rtb_Add1;
  rtY.info[6] = rtb_Add_l;
  rtY.info[7] = rtB.Int;
  rtY.info[8] = rtb_Int1;
  rtY.info[9] = rtb_Gain2_f;
  rtY.info[10] = rtb_Product;
  rtY.info[11] = rtb_Add_m;
  rtY.info[12] = rtb_Gain1_p;
  rtY.info[13] = rtb_Gain2_l;
  rtY.info[14] = ((rtb_Product + rtb_Add_m) + rtb_Gain1_p) + rtb_Gain2_l;

  /* Product: '<S4>/Divide' incorporates:
   *  Gain: '<S4>/Gain5'
   *  Inport: '<Root>/LoadTorque'
   *  Product: '<S4>/Divide1'
   *  Product: '<S4>/Divide2'
   *  Sum: '<S4>/Sum1'
   *  Trigonometry: '<S4>/Trigonometric Function'
   */
  rtB.Divide = (((rtb_Gain2_f - rtU.LoadTorque) - rtB.Int * 0.0004924) - tanh
                (4.0 * rtB.Int) * 0.0) / 0.0027;

  /* SignalConversion generated from: '<S3>/Vector Concatenate' incorporates:
   *  Outport: '<Root>/phase_curr'
   */
  rtY.phase_curr[2] = rtb_Subtract2;

  /* SignalConversion generated from: '<S3>/Vector Concatenate' incorporates:
   *  Outport: '<Root>/phase_curr'
   */
  rtY.phase_curr[1] = rtb_Gain4;

  /* SignalConversion generated from: '<S3>/Vector Concatenate' incorporates:
   *  Outport: '<Root>/phase_curr'
   */
  rtY.phase_curr[0] = rtb_Add1_a;

  /* Outport: '<Root>/mot_speed' */
  rtY.mot_speed = rtB.Int;
  UNUSED_PARAMETER(tid);
}

/* Update for root system: '<Root>' */
void MdlUpdate(int_T tid)
{
  /* Update for Integrator: '<S19>/Integrator' */
  rtDW.Integrator_IWORK = 0;

  /* Update for Integrator: '<S18>/Integrator' */
  rtDW.Integrator_IWORK_n = 0;
  UNUSED_PARAMETER(tid);
}

/* Derivatives for root system: '<Root>' */
void MdlDerivatives(void)
{
  boolean_T lsat;
  boolean_T usat;
  XDot *_rtXdot;
  _rtXdot = ((XDot *) ssGetdX(rtS));

  /* Derivatives for Integrator: '<S19>/Integrator' */
  lsat = (rtX.Integrator_CSTATE <= -1.0E+7);
  usat = (rtX.Integrator_CSTATE >= 1.0E+7);
  if (((!lsat) && (!usat)) || (lsat && (rtB.Add > 0.0)) || (usat && (rtB.Add <
        0.0))) {
    _rtXdot->Integrator_CSTATE = rtB.Add;
  } else {
    /* in saturation */
    _rtXdot->Integrator_CSTATE = 0.0;
  }

  /* End of Derivatives for Integrator: '<S19>/Integrator' */

  /* Derivatives for Integrator: '<S4>/Int1' */
  _rtXdot->Int1_CSTATE = rtB.Int;

  /* Derivatives for Integrator: '<S18>/Integrator' */
  lsat = (rtX.Integrator_CSTATE_o <= -1.0E+7);
  usat = (rtX.Integrator_CSTATE_o >= 1.0E+7);
  if (((!lsat) && (!usat)) || (lsat && (rtB.Add_i > 0.0)) || (usat && (rtB.Add_i
        < 0.0))) {
    _rtXdot->Integrator_CSTATE_o = rtB.Add_i;
  } else {
    /* in saturation */
    _rtXdot->Integrator_CSTATE_o = 0.0;
  }

  /* End of Derivatives for Integrator: '<S18>/Integrator' */

  /* Derivatives for Integrator: '<S4>/Int' */
  lsat = (rtX.Int_CSTATE <= -1.0E+7);
  usat = (rtX.Int_CSTATE >= 1.0E+7);
  if (((!lsat) && (!usat)) || (lsat && (rtB.Divide > 0.0)) || (usat &&
       (rtB.Divide < 0.0))) {
    _rtXdot->Int_CSTATE = rtB.Divide;
  } else {
    /* in saturation */
    _rtXdot->Int_CSTATE = 0.0;
  }

  /* End of Derivatives for Integrator: '<S4>/Int' */
}

/* Projection for root system: '<Root>' */
void MdlProjection(void)
{
}

/* Termination for root system: '<Root>' */
void MdlTerminate(void)
{
}

/* Function to initialize sizes */
void MdlInitializeSizes(void)
{
  ssSetNumContStates(rtS, 4);          /* Number of continuous states */
  ssSetNumPeriodicContStates(rtS, 1); /* Number of periodic continuous states */
  ssSetNumY(rtS, 19);                  /* Number of model outputs */
  ssSetNumU(rtS, 4);                   /* Number of model inputs */
  ssSetDirectFeedThrough(rtS, 1);      /* The model is direct feedthrough */
  ssSetNumSampleTimes(rtS, 2);         /* Number of sample times */
  ssSetNumBlocks(rtS, 128);            /* Number of blocks */
  ssSetNumBlockIO(rtS, 4);             /* Number of block outputs */
}

/* Function to initialize sample times. */
void MdlInitializeSampleTimes(void)
{
  /* task periods */
  ssSetSampleTime(rtS, 0, 0.0);
  ssSetSampleTime(rtS, 1, 0.2);

  /* task offsets */
  ssSetOffsetTime(rtS, 0, 0.0);
  ssSetOffsetTime(rtS, 1, 0.0);
}

/* Function to register the model */
/* Turns off all optimizations on Windows because of issues with VC 2015 compiler.
   This function is not performance-critical, hence this is not a problem.
 */
#if defined(_MSC_VER)

#pragma optimize( "", off )

#endif

SimStruct * motormodell(void)
{
  static struct _ssMdlInfo mdlInfo;
  (void) memset((char *)rtS, 0,
                sizeof(SimStruct));
  (void) memset((char *)&mdlInfo, 0,
                sizeof(struct _ssMdlInfo));
  ssSetMdlInfoPtr(rtS, &mdlInfo);

  /* timing info */
  {
    static time_T mdlPeriod[NSAMPLE_TIMES];
    static time_T mdlOffset[NSAMPLE_TIMES];
    static time_T mdlTaskTimes[NSAMPLE_TIMES];
    static int_T mdlTsMap[NSAMPLE_TIMES];
    static int_T mdlSampleHits[NSAMPLE_TIMES];

    {
      int_T i;
      for (i = 0; i < NSAMPLE_TIMES; i++) {
        mdlPeriod[i] = 0.0;
        mdlOffset[i] = 0.0;
        mdlTaskTimes[i] = 0.0;
        mdlTsMap[i] = i;
        mdlSampleHits[i] = 1;
      }
    }

    ssSetSampleTimePtr(rtS, &mdlPeriod[0]);
    ssSetOffsetTimePtr(rtS, &mdlOffset[0]);
    ssSetSampleTimeTaskIDPtr(rtS, &mdlTsMap[0]);
    ssSetTPtr(rtS, &mdlTaskTimes[0]);
    ssSetSampleHitPtr(rtS, &mdlSampleHits[0]);
  }

  ssSetSolverMode(rtS, SOLVER_MODE_SINGLETASKING);

  /*
   * initialize model vectors and cache them in SimStruct
   */

  /* block I/O */
  {
    ssSetBlockIO(rtS, ((void *) &rtB));
    (void) memset(((void *) &rtB), 0,
                  sizeof(B));
  }

  /* external inputs */
  {
    ssSetU(rtS, ((void*) &rtU));
    (void)memset(&rtU, 0, sizeof(ExtU));
  }

  /* external outputs */
  {
    ssSetY(rtS, &rtY);
    (void) memset((void *)&rtY, 0,
                  sizeof(ExtY));
  }

  /* states (continuous)*/
  {
    real_T *x = (real_T *) &rtX;
    ssSetContStates(rtS, x);
    (void) memset((void *)x, 0,
                  sizeof(X));
  }

  /* states (dwork) */
  {
    void *dwork = (void *) &rtDW;
    ssSetRootDWork(rtS, dwork);
    (void) memset(dwork, 0,
                  sizeof(DW));
  }

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    ssSetModelMappingInfo(rtS, &dtInfo);
    dtInfo.numDataTypes = 14;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.BTransTable = &rtBTransTable;
  }

  /* Model specific registration */
  ssSetRootSS(rtS, rtS);
  ssSetVersion(rtS, SIMSTRUCT_VERSION_LEVEL2);
  ssSetModelName(rtS, "motormodell");
  ssSetPath(rtS, "motormodell");
  ssSetTStart(rtS, 0.0);
  ssSetTFinal(rtS, 10.0);
  ssSetStepSize(rtS, 0.2);
  ssSetFixedStepSize(rtS, 0.2);

  /* Setup for data logging */
  {
    static RTWLogInfo rt_DataLoggingInfo;
    rt_DataLoggingInfo.loggingInterval = NULL;
    ssSetRTWLogInfo(rtS, &rt_DataLoggingInfo);
  }

  /* Setup for data logging */
  {
    rtliSetLogXSignalInfo(ssGetRTWLogInfo(rtS), (NULL));
    rtliSetLogXSignalPtrs(ssGetRTWLogInfo(rtS), (NULL));
    rtliSetLogT(ssGetRTWLogInfo(rtS), "tout");
    rtliSetLogX(ssGetRTWLogInfo(rtS), "");
    rtliSetLogXFinal(ssGetRTWLogInfo(rtS), "");
    rtliSetLogVarNameModifier(ssGetRTWLogInfo(rtS), "rt_");
    rtliSetLogFormat(ssGetRTWLogInfo(rtS), 4);
    rtliSetLogMaxRows(ssGetRTWLogInfo(rtS), 0);
    rtliSetLogDecimation(ssGetRTWLogInfo(rtS), 1);
    rtliSetLogY(ssGetRTWLogInfo(rtS), "");
    rtliSetLogYSignalInfo(ssGetRTWLogInfo(rtS), (NULL));
    rtliSetLogYSignalPtrs(ssGetRTWLogInfo(rtS), (NULL));
  }

  {
    static struct _ssStatesInfo2 statesInfo2;
    ssSetStatesInfo2(rtS, &statesInfo2);
  }

  {
    static ssPeriodicStatesInfo periodicStatesInfo;
    ssSetPeriodicStatesInfo(rtS, &periodicStatesInfo);
    ssSetPeriodicContStateIndices(rtS, rtPeriodicIndX);
    (void) memset((void*) rtPeriodicIndX, 0,
                  1*sizeof(int_T));
    ssSetPeriodicContStateRanges(rtS, rtPeriodicRngX);
    (void) memset((void*) rtPeriodicRngX, 0,
                  2*sizeof(real_T));
  }

  {
    static ssJacobianPerturbationBounds jacobianPerturbationBounds;
    ssSetJacobianPerturbationBounds(rtS, &jacobianPerturbationBounds);
  }

  ssSetChecksumVal(rtS, 0, 2655728346U);
  ssSetChecksumVal(rtS, 1, 510968949U);
  ssSetChecksumVal(rtS, 2, 2410199384U);
  ssSetChecksumVal(rtS, 3, 1821441005U);
  return rtS;
}

/* When you use the on parameter, it resets the optimizations to those that you
   specified with the /O compiler option. */
#if defined(_MSC_VER)

#pragma optimize( "", on )

#endif
