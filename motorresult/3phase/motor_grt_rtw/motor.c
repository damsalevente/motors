/*
 * motor.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "motor".
 *
 * Model version              : 1.2
 * Simulink Coder version : 9.3 (R2020a) 18-Nov-2019
 * C source code generated on : Wed Aug 26 17:23:56 2020
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "motor.h"
#include "motor_private.h"

/* Block signals (default storage) */
B_motor_T motor_B;

/* Block states (default storage) */
DW_motor_T motor_DW;

/* External inputs (root inport signals with default storage) */
ExtU_motor_T motor_U;

/* External outputs (root outports fed by signals with default storage) */
ExtY_motor_T motor_Y;

/* Real-time model */
RT_MODEL_motor_T motor_M_;
RT_MODEL_motor_T *const motor_M = &motor_M_;

/* Model step function */
void motor_step(void)
{
  real_T sinOut;
  real_T cosOut;

  /* MultiPortSwitch: '<S22>/Index Vector' incorporates:
   *  Constant: '<S22>/Constant'
   *  Constant: '<S22>/Constant1'
   */
  motor_B.IndexVector = motor_P.InteriorPMSM_idq0[(int32_T)
    motor_P.Constant1_Value - 1];

  /* DiscreteIntegrator: '<S22>/Discrete-Time Integrator3' */
  if (motor_DW.DiscreteTimeIntegrator3_IC_LOAD != 0) {
    motor_DW.DiscreteTimeIntegrator3_DSTATE = motor_B.IndexVector;
    if (motor_DW.DiscreteTimeIntegrator3_DSTATE >=
        motor_P.DiscreteTimeIntegrator3_UpperSa) {
      motor_DW.DiscreteTimeIntegrator3_DSTATE =
        motor_P.DiscreteTimeIntegrator3_UpperSa;
    } else {
      if (motor_DW.DiscreteTimeIntegrator3_DSTATE <=
          motor_P.DiscreteTimeIntegrator3_LowerSa) {
        motor_DW.DiscreteTimeIntegrator3_DSTATE =
          motor_P.DiscreteTimeIntegrator3_LowerSa;
      }
    }
  }

  if (motor_DW.DiscreteTimeIntegrator3_DSTATE >=
      motor_P.DiscreteTimeIntegrator3_UpperSa) {
    motor_DW.DiscreteTimeIntegrator3_DSTATE =
      motor_P.DiscreteTimeIntegrator3_UpperSa;
  } else {
    if (motor_DW.DiscreteTimeIntegrator3_DSTATE <=
        motor_P.DiscreteTimeIntegrator3_LowerSa) {
      motor_DW.DiscreteTimeIntegrator3_DSTATE =
        motor_P.DiscreteTimeIntegrator3_LowerSa;
    }
  }

  motor_B.DiscreteTimeIntegrator3 = motor_DW.DiscreteTimeIntegrator3_DSTATE;

  /* End of DiscreteIntegrator: '<S22>/Discrete-Time Integrator3' */

  /* UnitDelay: '<S8>/Unit Delay' */
  motor_B.UnitDelay = motor_DW.UnitDelay_DSTATE;

  /* RelationalOperator: '<S10>/Compare' incorporates:
   *  Constant: '<S10>/Constant'
   */
  motor_B.Compare = (motor_B.UnitDelay >= motor_P.Int_L);

  /* Switch: '<S8>/Switch1' */
  if (motor_B.Compare) {
    /* RelationalOperator: '<S9>/Compare' incorporates:
     *  Constant: '<S9>/Constant'
     */
    motor_B.Compare_f = (motor_B.UnitDelay <= motor_P.Int_U);

    /* Switch: '<S8>/Switch' */
    if (motor_B.Compare_f) {
      motor_B.Switch_m = motor_B.UnitDelay;
    } else {
      /* Sum: '<S8>/Subtract' incorporates:
       *  Constant: '<S8>/Constant3'
       */
      motor_B.Subtract = motor_B.UnitDelay - motor_P.Constant3_Value;
      motor_B.Switch_m = motor_B.Subtract;
    }

    /* End of Switch: '<S8>/Switch' */
    motor_B.Switch1 = motor_B.Switch_m;
  } else {
    /* Sum: '<S8>/Add1' incorporates:
     *  Constant: '<S8>/Constant1'
     */
    motor_B.Add1_o = motor_B.UnitDelay + motor_P.Constant1_Value_cx;
    motor_B.Switch1 = motor_B.Add1_o;
  }

  /* End of Switch: '<S8>/Switch1' */

  /* Gain: '<S5>/Gain4' */
  motor_B.Gain4 = motor_P.InteriorPMSM_P * motor_B.Switch1;

  /* Trigonometry: '<S7>/sine_cosine' */
  cosOut = motor_B.Gain4;
  sinOut = sin(cosOut);
  cosOut = cos(cosOut);
  motor_B.sine_cosine_o1 = sinOut;
  motor_B.sine_cosine_o2 = cosOut;

  /* Product: '<S17>/Product2' */
  motor_B.Product2 = motor_B.DiscreteTimeIntegrator3 * motor_B.sine_cosine_o2;

  /* MultiPortSwitch: '<S23>/Index Vector' incorporates:
   *  Constant: '<S23>/Constant'
   *  Constant: '<S23>/Constant1'
   */
  motor_B.IndexVector_k = motor_P.InteriorPMSM_idq0[(int32_T)
    motor_P.Constant1_Value_c - 1];

  /* DiscreteIntegrator: '<S23>/Discrete-Time Integrator3' */
  if (motor_DW.DiscreteTimeIntegrator3_IC_LO_k != 0) {
    motor_DW.DiscreteTimeIntegrator3_DSTAT_k = motor_B.IndexVector_k;
    if (motor_DW.DiscreteTimeIntegrator3_DSTAT_k >=
        motor_P.DiscreteTimeIntegrator3_Upper_d) {
      motor_DW.DiscreteTimeIntegrator3_DSTAT_k =
        motor_P.DiscreteTimeIntegrator3_Upper_d;
    } else {
      if (motor_DW.DiscreteTimeIntegrator3_DSTAT_k <=
          motor_P.DiscreteTimeIntegrator3_Lower_d) {
        motor_DW.DiscreteTimeIntegrator3_DSTAT_k =
          motor_P.DiscreteTimeIntegrator3_Lower_d;
      }
    }
  }

  if (motor_DW.DiscreteTimeIntegrator3_DSTAT_k >=
      motor_P.DiscreteTimeIntegrator3_Upper_d) {
    motor_DW.DiscreteTimeIntegrator3_DSTAT_k =
      motor_P.DiscreteTimeIntegrator3_Upper_d;
  } else {
    if (motor_DW.DiscreteTimeIntegrator3_DSTAT_k <=
        motor_P.DiscreteTimeIntegrator3_Lower_d) {
      motor_DW.DiscreteTimeIntegrator3_DSTAT_k =
        motor_P.DiscreteTimeIntegrator3_Lower_d;
    }
  }

  motor_B.DiscreteTimeIntegrator3_e = motor_DW.DiscreteTimeIntegrator3_DSTAT_k;

  /* End of DiscreteIntegrator: '<S23>/Discrete-Time Integrator3' */

  /* Product: '<S17>/Product3' */
  motor_B.Product3 = motor_B.DiscreteTimeIntegrator3_e * motor_B.sine_cosine_o1;

  /* Sum: '<S17>/Add1' */
  motor_B.Add1 = motor_B.Product2 - motor_B.Product3;

  /* Gain: '<S16>/Gain2' */
  motor_B.Gain2 = motor_P.Gain2_Gain * motor_B.Add1;

  /* Product: '<S17>/Product' */
  motor_B.Product = motor_B.DiscreteTimeIntegrator3 * motor_B.sine_cosine_o1;

  /* Product: '<S17>/Product1' */
  motor_B.Product1 = motor_B.DiscreteTimeIntegrator3_e * motor_B.sine_cosine_o2;

  /* Sum: '<S17>/Add' */
  motor_B.Add = motor_B.Product + motor_B.Product1;

  /* Gain: '<S16>/Gain3' */
  motor_B.Gain3 = motor_P.Gain3_Gain * motor_B.Add;

  /* Sum: '<S16>/Subtract1' */
  motor_B.Subtract1 = motor_B.Gain2 + motor_B.Gain3;

  /* Gain: '<S16>/Gain1' */
  motor_B.Gain1 = motor_P.Gain1_Gain * motor_B.Add1;

  /* Gain: '<S16>/Gain4' */
  motor_B.Gain4_j = motor_P.Gain4_Gain * motor_B.Add;

  /* Sum: '<S16>/Subtract2' */
  motor_B.Subtract2 = motor_B.Gain1 + motor_B.Gain4_j;

  /* Saturate: '<S1>/Saturation2' incorporates:
   *  Inport: '<Root>/In1'
   */
  cosOut = motor_U.In1[0];
  if (cosOut > motor_P.Saturation2_UpperSat) {
    cosOut = motor_P.Saturation2_UpperSat;
  } else {
    if (cosOut < motor_P.Saturation2_LowerSat) {
      cosOut = motor_P.Saturation2_LowerSat;
    }
  }

  motor_B.Saturation2[0] = cosOut;
  cosOut = motor_U.In1[1];
  if (cosOut > motor_P.Saturation2_UpperSat) {
    cosOut = motor_P.Saturation2_UpperSat;
  } else {
    if (cosOut < motor_P.Saturation2_LowerSat) {
      cosOut = motor_P.Saturation2_LowerSat;
    }
  }

  motor_B.Saturation2[1] = cosOut;
  cosOut = motor_U.In1[2];
  if (cosOut > motor_P.Saturation2_UpperSat) {
    cosOut = motor_P.Saturation2_UpperSat;
  } else {
    if (cosOut < motor_P.Saturation2_LowerSat) {
      cosOut = motor_P.Saturation2_LowerSat;
    }
  }

  motor_B.Saturation2[2] = cosOut;

  /* End of Saturate: '<S1>/Saturation2' */

  /* Sum: '<S1>/Sum' */
  motor_B.Sum = (motor_B.Saturation2[2] + motor_B.Saturation2[1]) +
    motor_B.Saturation2[0];

  /* Gain: '<S1>/Gain' */
  motor_B.Gain = motor_P.Gain_Gain * motor_B.Sum;

  /* Sum: '<S1>/Sum1' */
  motor_B.Sum1 = motor_B.Saturation2[0] - motor_B.Gain;

  /* Sum: '<S1>/Sum2' */
  motor_B.Sum2 = motor_B.Saturation2[1] - motor_B.Gain;

  /* Sum: '<S1>/Sum3' */
  motor_B.Sum3 = motor_B.Saturation2[2] - motor_B.Gain;

  /* Switch: '<S1>/Switch' incorporates:
   *  Constant: '<S1>/Constant'
   *  Inport: '<Root>/Vdc'
   */
  if (motor_U.Vdc >= motor_P.Switch_Threshold) {
    motor_B.Switch = motor_U.Vdc;
  } else {
    motor_B.Switch = motor_P.Constant_Value;
  }

  /* End of Switch: '<S1>/Switch' */

  /* Product: '<S1>/Product' */
  motor_B.Product_l[0] = motor_B.Sum1 * motor_B.Switch;
  motor_B.Product_l[1] = motor_B.Sum2 * motor_B.Switch;
  motor_B.Product_l[2] = motor_B.Sum3 * motor_B.Switch;

  /* Gain: '<S15>/Gain' */
  motor_B.Gain_f = motor_P.Gain_Gain_k * motor_B.Product_l[0];

  /* Gain: '<S15>/Gain1' */
  motor_B.Gain1_p = motor_P.Gain1_Gain_o * motor_B.Product_l[1];

  /* Gain: '<S15>/Gain4' */
  motor_B.Gain4_o = motor_P.Gain4_Gain_n * motor_B.Product_l[2];

  /* Sum: '<S15>/Add' */
  motor_B.Add_j = (motor_B.Gain_f + motor_B.Gain1_p) + motor_B.Gain4_o;

  /* Product: '<S20>/Product2' */
  motor_B.Product2_g = motor_B.Add_j * motor_B.sine_cosine_o2;

  /* Gain: '<S15>/Gain2' */
  motor_B.Gain2_n = motor_P.Gain2_Gain_k * motor_B.Product_l[1];

  /* Gain: '<S15>/Gain3' */
  motor_B.Gain3_f = motor_P.Gain3_Gain_i * motor_B.Product_l[2];

  /* Sum: '<S15>/Add1' */
  motor_B.Add1_h = motor_B.Gain2_n + motor_B.Gain3_f;

  /* Product: '<S20>/Product3' */
  motor_B.Product3_m = motor_B.Add1_h * motor_B.sine_cosine_o1;

  /* Sum: '<S20>/Add1' */
  motor_B.Add1_m = motor_B.Product2_g + motor_B.Product3_m;

  /* Product: '<S20>/Product' */
  motor_B.Product_n = motor_B.Add_j * motor_B.sine_cosine_o1;

  /* Product: '<S20>/Product1' */
  motor_B.Product1_n = motor_B.Add1_h * motor_B.sine_cosine_o2;

  /* Sum: '<S20>/Add' */
  motor_B.Add_b = motor_B.Product1_n - motor_B.Product_n;

  /* Product: '<S18>/Product' */
  motor_B.Product_j = motor_B.DiscreteTimeIntegrator3 *
    motor_B.DiscreteTimeIntegrator3_e;

  /* MultiPortSwitch: '<S21>/Index Vector' incorporates:
   *  Constant: '<S21>/Constant'
   *  Constant: '<S21>/Constant1'
   */
  motor_B.IndexVector_p = motor_P.InteriorPMSM_Ldq[(int32_T)
    motor_P.Constant1_Value_l - 1];

  /* MultiPortSwitch: '<S21>/Index Vector1' incorporates:
   *  Constant: '<S21>/Constant'
   *  Constant: '<S21>/Constant2'
   */
  motor_B.IndexVector1 = motor_P.InteriorPMSM_Ldq[(int32_T)
    motor_P.Constant2_Value - 1];

  /* Sum: '<S21>/Add' */
  motor_B.Add_i = motor_B.IndexVector_p - motor_B.IndexVector1;

  /* Product: '<S21>/Product' */
  motor_B.Product_b = motor_B.Product_j * motor_B.Add_i;

  /* Gain: '<S18>/Gain1' */
  motor_B.Gain1_f = motor_P.InteriorPMSM_lambda_pm_calc *
    motor_B.DiscreteTimeIntegrator3_e;

  /* Sum: '<S18>/Add' */
  motor_B.Add_p = motor_B.Product_b + motor_B.Gain1_f;

  /* Gain: '<S18>/Gain2' */
  cosOut = 1.5 * motor_P.InteriorPMSM_P;
  motor_B.Gain2_c = cosOut * motor_B.Add_p;

  /* Product: '<S6>/Product3' incorporates:
   *  Inport: '<Root>/In2'
   */
  motor_B.Product3_i = motor_U.In2 * motor_B.Gain2_c;

  /* Gain: '<S6>/Gain' */
  motor_B.Gain_n = motor_P.Gain_Gain_f * motor_B.Product3_i;

  /* Product: '<S6>/Product' */
  motor_B.Product_ny = motor_B.Add1 * motor_B.Product_l[0];

  /* Product: '<S6>/Product1' */
  motor_B.Product1_p = motor_B.Subtract1 * motor_B.Product_l[1];

  /* Product: '<S6>/Product2' */
  motor_B.Product2_h = motor_B.Subtract2 * motor_B.Product_l[2];

  /* Sum: '<S6>/Add' */
  motor_B.Add_e = (motor_B.Product_ny + motor_B.Product1_p) + motor_B.Product2_h;

  /* Product: '<S6>/Product11' */
  motor_B.Product11 = motor_B.DiscreteTimeIntegrator3 *
    motor_B.DiscreteTimeIntegrator3;

  /* Product: '<S6>/Product12' */
  motor_B.Product12 = motor_B.DiscreteTimeIntegrator3_e *
    motor_B.DiscreteTimeIntegrator3_e;

  /* Sum: '<S6>/Add2' */
  motor_B.Add2 = motor_B.Product11 + motor_B.Product12;

  /* Product: '<S6>/Product4' incorporates:
   *  Constant: '<S6>/Constant'
   */
  motor_B.Product4 = motor_B.Add2 * motor_P.InteriorPMSM_Rs;

  /* Gain: '<S6>/Gain1' */
  motor_B.Gain1_pl = motor_P.Gain1_Gain_n * motor_B.Product4;

  /* Gain: '<S6>/Gain2' incorporates:
   *  Constant: '<S5>/Constant'
   */
  motor_B.Gain2_o = motor_P.Gain2_Gain_l * motor_P.Constant_Value_i;

  /* Sum: '<S6>/Add4' */
  motor_B.Add4 = ((motor_B.Gain_n + motor_B.Add_e) + motor_B.Gain1_pl) +
    motor_B.Gain2_o;

  /* Outport: '<Root>/Out1' incorporates:
   *  Inport: '<Root>/In2'
   */
  motor_Y.Out1[0] = motor_B.Add1;
  motor_Y.Out1[1] = motor_B.Subtract1;
  motor_Y.Out1[2] = motor_B.Subtract2;
  motor_Y.Out1[3] = motor_B.DiscreteTimeIntegrator3;
  motor_Y.Out1[4] = motor_B.DiscreteTimeIntegrator3_e;
  motor_Y.Out1[5] = motor_B.Add1_m;
  motor_Y.Out1[6] = motor_B.Add_b;
  motor_Y.Out1[7] = motor_U.In2;
  motor_Y.Out1[8] = motor_B.Switch1;
  motor_Y.Out1[9] = motor_B.Gain2_c;
  motor_Y.Out1[10] = motor_B.Gain_n;
  motor_Y.Out1[11] = motor_B.Add_e;
  motor_Y.Out1[12] = motor_B.Gain1_pl;
  motor_Y.Out1[13] = motor_B.Gain2_o;
  motor_Y.Out1[14] = motor_B.Add4;

  /* SignalConversion generated from: '<S4>/Vector Concatenate' */
  motor_B.VectorConcatenate[0] = motor_B.Add1;

  /* SignalConversion generated from: '<S4>/Vector Concatenate' */
  motor_B.VectorConcatenate[1] = motor_B.Subtract1;

  /* SignalConversion generated from: '<S4>/Vector Concatenate' */
  motor_B.VectorConcatenate[2] = motor_B.Subtract2;

  /* Outport: '<Root>/Out2' */
  motor_Y.Out2[0] = motor_B.VectorConcatenate[0];
  motor_Y.Out2[1] = motor_B.VectorConcatenate[1];
  motor_Y.Out2[2] = motor_B.VectorConcatenate[2];

  /* Outport: '<Root>/Out3' */
  motor_Y.Out3 = motor_B.Gain2_c;

  /* Gain: '<S5>/Gain' incorporates:
   *  Inport: '<Root>/In2'
   */
  motor_B.Gain_n0 = motor_P.InteriorPMSM_P * motor_U.In2;

  /* Gain: '<S8>/Gain1' incorporates:
   *  Inport: '<Root>/In2'
   */
  motor_B.Gain1_m = motor_P.Int_Ts * motor_U.In2;

  /* Sum: '<S8>/Add' */
  motor_B.Add_a = motor_B.Gain1_m + motor_B.Switch1;

  /* MultiPortSwitch: '<S24>/Index Vector' incorporates:
   *  Constant: '<S24>/Constant'
   *  Constant: '<S24>/Constant1'
   */
  motor_B.IndexVector_b = motor_P.InteriorPMSM_Ldq[(int32_T)
    motor_P.Constant1_Value_k - 1];

  /* Product: '<S24>/Product' */
  motor_B.Product_c = motor_B.Add1_m / motor_B.IndexVector_b;

  /* Product: '<S22>/Product' */
  motor_B.Product_l3 = motor_B.Gain_n0 * motor_B.DiscreteTimeIntegrator3_e;

  /* MultiPortSwitch: '<S25>/Index Vector' incorporates:
   *  Constant: '<S25>/Constant'
   *  Constant: '<S25>/Constant1'
   */
  motor_B.IndexVector_f = motor_P.InteriorPMSM_Ldq[(int32_T)
    motor_P.Constant1_Value_i - 1];

  /* MultiPortSwitch: '<S25>/Index Vector1' incorporates:
   *  Constant: '<S25>/Constant'
   *  Constant: '<S25>/Constant2'
   */
  motor_B.IndexVector1_b = motor_P.InteriorPMSM_Ldq[(int32_T)
    motor_P.Constant2_Value_l - 1];

  /* Product: '<S25>/Product' */
  motor_B.Product_g = motor_B.Product_l3 * motor_B.IndexVector_f /
    motor_B.IndexVector1_b;

  /* MultiPortSwitch: '<S26>/Index Vector' incorporates:
   *  Constant: '<S26>/Constant1'
   *  Constant: '<S26>/Constant2'
   */
  motor_B.IndexVector_pa = motor_P.InteriorPMSM_Ldq[(int32_T)
    motor_P.Constant2_Value_i - 1];

  /* Product: '<S26>/Product' incorporates:
   *  Constant: '<S26>/Constant'
   */
  motor_B.Product_m = motor_B.DiscreteTimeIntegrator3 * motor_P.InteriorPMSM_Rs /
    motor_B.IndexVector_pa;

  /* Sum: '<S22>/Add' */
  motor_B.Add_l = (motor_B.Product_c + motor_B.Product_g) - motor_B.Product_m;

  /* MultiPortSwitch: '<S27>/Index Vector' incorporates:
   *  Constant: '<S27>/Constant'
   *  Constant: '<S27>/Constant1'
   */
  motor_B.IndexVector_c = motor_P.InteriorPMSM_Ldq[(int32_T)
    motor_P.Constant1_Value_m - 1];

  /* Product: '<S27>/Product' */
  motor_B.Product_d = motor_B.Add_b / motor_B.IndexVector_c;

  /* Product: '<S23>/Product' */
  motor_B.Product_o = motor_B.DiscreteTimeIntegrator3 * motor_B.Gain_n0;

  /* MultiPortSwitch: '<S28>/Index Vector' incorporates:
   *  Constant: '<S28>/Constant'
   *  Constant: '<S28>/Constant1'
   */
  motor_B.IndexVector_g = motor_P.InteriorPMSM_Ldq[(int32_T)
    motor_P.Constant1_Value_d - 1];

  /* MultiPortSwitch: '<S28>/Index Vector1' incorporates:
   *  Constant: '<S28>/Constant'
   *  Constant: '<S28>/Constant2'
   */
  motor_B.IndexVector1_c = motor_P.InteriorPMSM_Ldq[(int32_T)
    motor_P.Constant2_Value_o - 1];

  /* Product: '<S28>/Product' */
  motor_B.Product_p = motor_B.Product_o * motor_B.IndexVector_g /
    motor_B.IndexVector1_c;

  /* MultiPortSwitch: '<S29>/Index Vector' incorporates:
   *  Constant: '<S29>/Constant1'
   *  Constant: '<S29>/Constant2'
   */
  motor_B.IndexVector_bo = motor_P.InteriorPMSM_Ldq[(int32_T)
    motor_P.Constant2_Value_d - 1];

  /* Product: '<S29>/Product' incorporates:
   *  Constant: '<S29>/Constant'
   */
  motor_B.Product_jy = motor_B.Gain_n0 * motor_P.InteriorPMSM_lambda_pm_calc /
    motor_B.IndexVector_bo;

  /* MultiPortSwitch: '<S30>/Index Vector' incorporates:
   *  Constant: '<S30>/Constant1'
   *  Constant: '<S30>/Constant2'
   */
  motor_B.IndexVector_kq = motor_P.InteriorPMSM_Ldq[(int32_T)
    motor_P.Constant2_Value_dv - 1];

  /* Product: '<S30>/Product' incorporates:
   *  Constant: '<S30>/Constant'
   */
  motor_B.Product_g3 = motor_B.DiscreteTimeIntegrator3_e *
    motor_P.InteriorPMSM_Rs / motor_B.IndexVector_kq;

  /* Sum: '<S23>/Add' */
  motor_B.Add_pf = ((motor_B.Product_d - motor_B.Product_p) - motor_B.Product_jy)
    - motor_B.Product_g3;

  /* Update for DiscreteIntegrator: '<S22>/Discrete-Time Integrator3' */
  motor_DW.DiscreteTimeIntegrator3_IC_LOAD = 0U;
  motor_DW.DiscreteTimeIntegrator3_DSTATE +=
    motor_P.DiscreteTimeIntegrator3_gainval * motor_B.Add_l;
  if (motor_DW.DiscreteTimeIntegrator3_DSTATE >=
      motor_P.DiscreteTimeIntegrator3_UpperSa) {
    motor_DW.DiscreteTimeIntegrator3_DSTATE =
      motor_P.DiscreteTimeIntegrator3_UpperSa;
  } else {
    if (motor_DW.DiscreteTimeIntegrator3_DSTATE <=
        motor_P.DiscreteTimeIntegrator3_LowerSa) {
      motor_DW.DiscreteTimeIntegrator3_DSTATE =
        motor_P.DiscreteTimeIntegrator3_LowerSa;
    }
  }

  /* End of Update for DiscreteIntegrator: '<S22>/Discrete-Time Integrator3' */

  /* Update for UnitDelay: '<S8>/Unit Delay' */
  motor_DW.UnitDelay_DSTATE = motor_B.Add_a;

  /* Update for DiscreteIntegrator: '<S23>/Discrete-Time Integrator3' */
  motor_DW.DiscreteTimeIntegrator3_IC_LO_k = 0U;
  motor_DW.DiscreteTimeIntegrator3_DSTAT_k +=
    motor_P.DiscreteTimeIntegrator3_gainv_i * motor_B.Add_pf;
  if (motor_DW.DiscreteTimeIntegrator3_DSTAT_k >=
      motor_P.DiscreteTimeIntegrator3_Upper_d) {
    motor_DW.DiscreteTimeIntegrator3_DSTAT_k =
      motor_P.DiscreteTimeIntegrator3_Upper_d;
  } else {
    if (motor_DW.DiscreteTimeIntegrator3_DSTAT_k <=
        motor_P.DiscreteTimeIntegrator3_Lower_d) {
      motor_DW.DiscreteTimeIntegrator3_DSTAT_k =
        motor_P.DiscreteTimeIntegrator3_Lower_d;
    }
  }

  /* End of Update for DiscreteIntegrator: '<S23>/Discrete-Time Integrator3' */

  /* Matfile logging */
  rt_UpdateTXYLogVars(motor_M->rtwLogInfo, (&motor_M->Timing.taskTime0));

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.001s, 0.0s] */
    if ((rtmGetTFinal(motor_M)!=-1) &&
        !((rtmGetTFinal(motor_M)-motor_M->Timing.taskTime0) >
          motor_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(motor_M, "Simulation finished");
    }
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++motor_M->Timing.clockTick0)) {
    ++motor_M->Timing.clockTickH0;
  }

  motor_M->Timing.taskTime0 = motor_M->Timing.clockTick0 *
    motor_M->Timing.stepSize0 + motor_M->Timing.clockTickH0 *
    motor_M->Timing.stepSize0 * 4294967296.0;
}

/* Model initialize function */
void motor_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)motor_M, 0,
                sizeof(RT_MODEL_motor_T));
  rtmSetTFinal(motor_M, -1);
  motor_M->Timing.stepSize0 = 0.001;

  /* Setup for data logging */
  {
    static RTWLogInfo rt_DataLoggingInfo;
    rt_DataLoggingInfo.loggingInterval = NULL;
    motor_M->rtwLogInfo = &rt_DataLoggingInfo;
  }

  /* Setup for data logging */
  {
    rtliSetLogXSignalInfo(motor_M->rtwLogInfo, (NULL));
    rtliSetLogXSignalPtrs(motor_M->rtwLogInfo, (NULL));
    rtliSetLogT(motor_M->rtwLogInfo, "tout");
    rtliSetLogX(motor_M->rtwLogInfo, "");
    rtliSetLogXFinal(motor_M->rtwLogInfo, "");
    rtliSetLogVarNameModifier(motor_M->rtwLogInfo, "rt_");
    rtliSetLogFormat(motor_M->rtwLogInfo, 4);
    rtliSetLogMaxRows(motor_M->rtwLogInfo, 0);
    rtliSetLogDecimation(motor_M->rtwLogInfo, 1);
    rtliSetLogY(motor_M->rtwLogInfo, "");
    rtliSetLogYSignalInfo(motor_M->rtwLogInfo, (NULL));
    rtliSetLogYSignalPtrs(motor_M->rtwLogInfo, (NULL));
  }

  /* block I/O */
  (void) memset(((void *) &motor_B), 0,
                sizeof(B_motor_T));

  /* states (dwork) */
  (void) memset((void *)&motor_DW, 0,
                sizeof(DW_motor_T));

  /* external inputs */
  (void)memset(&motor_U, 0, sizeof(ExtU_motor_T));

  /* external outputs */
  (void) memset((void *)&motor_Y, 0,
                sizeof(ExtY_motor_T));

  /* Matfile logging */
  rt_StartDataLoggingWithStartTime(motor_M->rtwLogInfo, 0.0, rtmGetTFinal
    (motor_M), motor_M->Timing.stepSize0, (&rtmGetErrorStatus(motor_M)));

  /* InitializeConditions for DiscreteIntegrator: '<S22>/Discrete-Time Integrator3' */
  motor_DW.DiscreteTimeIntegrator3_IC_LOAD = 1U;

  /* InitializeConditions for UnitDelay: '<S8>/Unit Delay' */
  motor_DW.UnitDelay_DSTATE = motor_P.InteriorPMSM_theta_init;

  /* InitializeConditions for DiscreteIntegrator: '<S23>/Discrete-Time Integrator3' */
  motor_DW.DiscreteTimeIntegrator3_IC_LO_k = 1U;
}

/* Model terminate function */
void motor_terminate(void)
{
  /* (no terminate code required) */
}
