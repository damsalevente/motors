/*
 * motor_data.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "motor".
 *
 * Model version              : 1.2
 * Simulink Coder version : 9.3 (R2020a) 18-Nov-2019
 * C source code generated on : Wed Aug 26 17:23:56 2020
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "motor.h"
#include "motor_private.h"

/* Block parameters (default storage) */
P_motor_T motor_P = {
  /* Mask Parameter: Int_L
   * Referenced by: '<S10>/Constant'
   */
  -3.1415926535897931,

  /* Mask Parameter: InteriorPMSM_Ldq
   * Referenced by:
   *   '<S21>/Constant'
   *   '<S24>/Constant'
   *   '<S25>/Constant'
   *   '<S26>/Constant1'
   *   '<S27>/Constant'
   *   '<S28>/Constant'
   *   '<S29>/Constant1'
   *   '<S30>/Constant1'
   */
  { 0.0017, 0.0032 },

  /* Mask Parameter: InteriorPMSM_P
   * Referenced by:
   *   '<S5>/Gain'
   *   '<S5>/Gain4'
   *   '<S18>/Gain2'
   */
  4.0,

  /* Mask Parameter: InteriorPMSM_Rs
   * Referenced by:
   *   '<S6>/Constant'
   *   '<S26>/Constant'
   *   '<S30>/Constant'
   */
  0.02,

  /* Mask Parameter: Int_Ts
   * Referenced by: '<S8>/Gain1'
   */
  0.001,

  /* Mask Parameter: Int_U
   * Referenced by: '<S9>/Constant'
   */
  3.1415926535897931,

  /* Mask Parameter: InteriorPMSM_idq0
   * Referenced by:
   *   '<S22>/Constant'
   *   '<S23>/Constant'
   */
  { 0.0, 0.0 },

  /* Mask Parameter: InteriorPMSM_lambda_pm_calc
   * Referenced by:
   *   '<S18>/Gain1'
   *   '<S29>/Constant'
   */
  0.2205,

  /* Mask Parameter: InteriorPMSM_theta_init
   * Referenced by: '<S8>/Unit Delay'
   */
  0.0,

  /* Expression: 0
   * Referenced by: '<S1>/Constant'
   */
  0.0,

  /* Expression: 1
   * Referenced by: '<S22>/Constant1'
   */
  1.0,

  /* Computed Parameter: DiscreteTimeIntegrator3_gainval
   * Referenced by: '<S22>/Discrete-Time Integrator3'
   */
  0.001,

  /* Expression: 10e6
   * Referenced by: '<S22>/Discrete-Time Integrator3'
   */
  1.0E+7,

  /* Expression: -10e6
   * Referenced by: '<S22>/Discrete-Time Integrator3'
   */
  -1.0E+7,

  /* Expression: 2
   * Referenced by: '<S23>/Constant1'
   */
  2.0,

  /* Computed Parameter: DiscreteTimeIntegrator3_gainv_i
   * Referenced by: '<S23>/Discrete-Time Integrator3'
   */
  0.001,

  /* Expression: 10e6
   * Referenced by: '<S23>/Discrete-Time Integrator3'
   */
  1.0E+7,

  /* Expression: -10e6
   * Referenced by: '<S23>/Discrete-Time Integrator3'
   */
  -1.0E+7,

  /* Expression: -1/2
   * Referenced by: '<S16>/Gain2'
   */
  -0.5,

  /* Expression: sqrt(3)/2
   * Referenced by: '<S16>/Gain3'
   */
  0.8660254037844386,

  /* Expression: -1/2
   * Referenced by: '<S16>/Gain1'
   */
  -0.5,

  /* Expression: -sqrt(3)/2
   * Referenced by: '<S16>/Gain4'
   */
  -0.8660254037844386,

  /* Expression: 1
   * Referenced by: '<S1>/Saturation2'
   */
  1.0,

  /* Expression: 0
   * Referenced by: '<S1>/Saturation2'
   */
  0.0,

  /* Expression: 1/3
   * Referenced by: '<S1>/Gain'
   */
  0.33333333333333331,

  /* Expression: 0
   * Referenced by: '<S1>/Switch'
   */
  0.0,

  /* Expression: 2/3
   * Referenced by: '<S15>/Gain'
   */
  0.66666666666666663,

  /* Expression: -1/3
   * Referenced by: '<S15>/Gain1'
   */
  -0.33333333333333331,

  /* Expression: -1/3
   * Referenced by: '<S15>/Gain4'
   */
  -0.33333333333333331,

  /* Expression: sqrt(3)/3
   * Referenced by: '<S15>/Gain2'
   */
  0.57735026918962573,

  /* Expression: -sqrt(3)/3
   * Referenced by: '<S15>/Gain3'
   */
  -0.57735026918962573,

  /* Expression: 1
   * Referenced by: '<S21>/Constant1'
   */
  1.0,

  /* Expression: 2
   * Referenced by: '<S21>/Constant2'
   */
  2.0,

  /* Expression: -1
   * Referenced by: '<S6>/Gain'
   */
  -1.0,

  /* Expression: -1
   * Referenced by: '<S6>/Gain1'
   */
  -1.0,

  /* Expression: 0
   * Referenced by: '<S5>/Constant'
   */
  0.0,

  /* Expression: -1
   * Referenced by: '<S6>/Gain2'
   */
  -1.0,

  /* Expression: 1
   * Referenced by: '<S24>/Constant1'
   */
  1.0,

  /* Expression: 2
   * Referenced by: '<S25>/Constant1'
   */
  2.0,

  /* Expression: 1
   * Referenced by: '<S25>/Constant2'
   */
  1.0,

  /* Expression: 1
   * Referenced by: '<S26>/Constant2'
   */
  1.0,

  /* Expression: 2
   * Referenced by: '<S27>/Constant1'
   */
  2.0,

  /* Expression: 1
   * Referenced by: '<S28>/Constant1'
   */
  1.0,

  /* Expression: 2
   * Referenced by: '<S28>/Constant2'
   */
  2.0,

  /* Expression: 2
   * Referenced by: '<S29>/Constant2'
   */
  2.0,

  /* Expression: 2
   * Referenced by: '<S30>/Constant2'
   */
  2.0,

  /* Computed Parameter: Constant1_Value_cx
   * Referenced by: '<S8>/Constant1'
   */
  6.28318548F,

  /* Computed Parameter: Constant3_Value
   * Referenced by: '<S8>/Constant3'
   */
  6.28318548F
};
