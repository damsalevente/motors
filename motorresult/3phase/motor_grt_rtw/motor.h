/*
 * motor.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "motor".
 *
 * Model version              : 1.2
 * Simulink Coder version : 9.3 (R2020a) 18-Nov-2019
 * C source code generated on : Wed Aug 26 17:23:56 2020
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_motor_h_
#define RTW_HEADER_motor_h_
#include <math.h>
#include <string.h>
#include <float.h>
#include <stddef.h>
#ifndef motor_COMMON_INCLUDES_
# define motor_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "rt_logging.h"
#endif                                 /* motor_COMMON_INCLUDES_ */

#include "motor_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rt_nonfinite.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetRTWLogInfo
# define rtmGetRTWLogInfo(rtm)         ((rtm)->rtwLogInfo)
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetTPtr
# define rtmGetTPtr(rtm)               (&(rtm)->Timing.taskTime0)
#endif

/* Block signals (default storage) */
typedef struct {
  real_T IndexVector;                  /* '<S22>/Index Vector' */
  real_T DiscreteTimeIntegrator3;      /* '<S22>/Discrete-Time Integrator3' */
  real_T UnitDelay;                    /* '<S8>/Unit Delay' */
  real_T Switch1;                      /* '<S8>/Switch1' */
  real_T Gain4;                        /* '<S5>/Gain4' */
  real_T sine_cosine_o1;               /* '<S7>/sine_cosine' */
  real_T sine_cosine_o2;               /* '<S7>/sine_cosine' */
  real_T Product2;                     /* '<S17>/Product2' */
  real_T IndexVector_k;                /* '<S23>/Index Vector' */
  real_T DiscreteTimeIntegrator3_e;    /* '<S23>/Discrete-Time Integrator3' */
  real_T Product3;                     /* '<S17>/Product3' */
  real_T Add1;                         /* '<S17>/Add1' */
  real_T Gain2;                        /* '<S16>/Gain2' */
  real_T Product;                      /* '<S17>/Product' */
  real_T Product1;                     /* '<S17>/Product1' */
  real_T Add;                          /* '<S17>/Add' */
  real_T Gain3;                        /* '<S16>/Gain3' */
  real_T Subtract1;                    /* '<S16>/Subtract1' */
  real_T Gain1;                        /* '<S16>/Gain1' */
  real_T Gain4_j;                      /* '<S16>/Gain4' */
  real_T Subtract2;                    /* '<S16>/Subtract2' */
  real_T Saturation2[3];               /* '<S1>/Saturation2' */
  real_T Sum;                          /* '<S1>/Sum' */
  real_T Gain;                         /* '<S1>/Gain' */
  real_T Sum1;                         /* '<S1>/Sum1' */
  real_T Sum2;                         /* '<S1>/Sum2' */
  real_T Sum3;                         /* '<S1>/Sum3' */
  real_T Switch;                       /* '<S1>/Switch' */
  real_T Product_l[3];                 /* '<S1>/Product' */
  real_T Gain_f;                       /* '<S15>/Gain' */
  real_T Gain1_p;                      /* '<S15>/Gain1' */
  real_T Gain4_o;                      /* '<S15>/Gain4' */
  real_T Add_j;                        /* '<S15>/Add' */
  real_T Product2_g;                   /* '<S20>/Product2' */
  real_T Gain2_n;                      /* '<S15>/Gain2' */
  real_T Gain3_f;                      /* '<S15>/Gain3' */
  real_T Add1_h;                       /* '<S15>/Add1' */
  real_T Product3_m;                   /* '<S20>/Product3' */
  real_T Add1_m;                       /* '<S20>/Add1' */
  real_T Product_n;                    /* '<S20>/Product' */
  real_T Product1_n;                   /* '<S20>/Product1' */
  real_T Add_b;                        /* '<S20>/Add' */
  real_T Product_j;                    /* '<S18>/Product' */
  real_T IndexVector_p;                /* '<S21>/Index Vector' */
  real_T IndexVector1;                 /* '<S21>/Index Vector1' */
  real_T Add_i;                        /* '<S21>/Add' */
  real_T Product_b;                    /* '<S21>/Product' */
  real_T Gain1_f;                      /* '<S18>/Gain1' */
  real_T Add_p;                        /* '<S18>/Add' */
  real_T Gain2_c;                      /* '<S18>/Gain2' */
  real_T Product3_i;                   /* '<S6>/Product3' */
  real_T Gain_n;                       /* '<S6>/Gain' */
  real_T Product_ny;                   /* '<S6>/Product' */
  real_T Product1_p;                   /* '<S6>/Product1' */
  real_T Product2_h;                   /* '<S6>/Product2' */
  real_T Add_e;                        /* '<S6>/Add' */
  real_T Product11;                    /* '<S6>/Product11' */
  real_T Product12;                    /* '<S6>/Product12' */
  real_T Add2;                         /* '<S6>/Add2' */
  real_T Product4;                     /* '<S6>/Product4' */
  real_T Gain1_pl;                     /* '<S6>/Gain1' */
  real_T Gain2_o;                      /* '<S6>/Gain2' */
  real_T Add4;                         /* '<S6>/Add4' */
  real_T VectorConcatenate[3];         /* '<S4>/Vector Concatenate' */
  real_T Gain_n0;                      /* '<S5>/Gain' */
  real_T Gain1_m;                      /* '<S8>/Gain1' */
  real_T Add_a;                        /* '<S8>/Add' */
  real_T IndexVector_b;                /* '<S24>/Index Vector' */
  real_T Product_c;                    /* '<S24>/Product' */
  real_T Product_l3;                   /* '<S22>/Product' */
  real_T IndexVector_f;                /* '<S25>/Index Vector' */
  real_T IndexVector1_b;               /* '<S25>/Index Vector1' */
  real_T Product_g;                    /* '<S25>/Product' */
  real_T IndexVector_pa;               /* '<S26>/Index Vector' */
  real_T Product_m;                    /* '<S26>/Product' */
  real_T Add_l;                        /* '<S22>/Add' */
  real_T IndexVector_c;                /* '<S27>/Index Vector' */
  real_T Product_d;                    /* '<S27>/Product' */
  real_T Product_o;                    /* '<S23>/Product' */
  real_T IndexVector_g;                /* '<S28>/Index Vector' */
  real_T IndexVector1_c;               /* '<S28>/Index Vector1' */
  real_T Product_p;                    /* '<S28>/Product' */
  real_T IndexVector_bo;               /* '<S29>/Index Vector' */
  real_T Product_jy;                   /* '<S29>/Product' */
  real_T IndexVector_kq;               /* '<S30>/Index Vector' */
  real_T Product_g3;                   /* '<S30>/Product' */
  real_T Add_pf;                       /* '<S23>/Add' */
  real_T Switch_m;                     /* '<S8>/Switch' */
  real_T Subtract;                     /* '<S8>/Subtract' */
  real_T Add1_o;                       /* '<S8>/Add1' */
  boolean_T Compare;                   /* '<S10>/Compare' */
  boolean_T Compare_f;                 /* '<S9>/Compare' */
} B_motor_T;

/* Block states (default storage) for system '<Root>' */
typedef struct {
  real_T DiscreteTimeIntegrator3_DSTATE;/* '<S22>/Discrete-Time Integrator3' */
  real_T UnitDelay_DSTATE;             /* '<S8>/Unit Delay' */
  real_T DiscreteTimeIntegrator3_DSTAT_k;/* '<S23>/Discrete-Time Integrator3' */
  real_T Subtract_DWORK1;              /* '<S8>/Subtract' */
  uint8_T DiscreteTimeIntegrator3_IC_LOAD;/* '<S22>/Discrete-Time Integrator3' */
  uint8_T DiscreteTimeIntegrator3_IC_LO_k;/* '<S23>/Discrete-Time Integrator3' */
} DW_motor_T;

/* External inputs (root inport signals with default storage) */
typedef struct {
  real_T Vdc;                          /* '<Root>/Vdc' */
  real_T In2;                          /* '<Root>/In2' */
  real_T In1[3];                       /* '<Root>/In1' */
} ExtU_motor_T;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  real_T Out1[15];                     /* '<Root>/Out1' */
  real_T Out2[3];                      /* '<Root>/Out2' */
  real_T Out3;                         /* '<Root>/Out3' */
} ExtY_motor_T;

/* Parameters (default storage) */
struct P_motor_T_ {
  real_T Int_L;                        /* Mask Parameter: Int_L
                                        * Referenced by: '<S10>/Constant'
                                        */
  real_T InteriorPMSM_Ldq[2];          /* Mask Parameter: InteriorPMSM_Ldq
                                        * Referenced by:
                                        *   '<S21>/Constant'
                                        *   '<S24>/Constant'
                                        *   '<S25>/Constant'
                                        *   '<S26>/Constant1'
                                        *   '<S27>/Constant'
                                        *   '<S28>/Constant'
                                        *   '<S29>/Constant1'
                                        *   '<S30>/Constant1'
                                        */
  real_T InteriorPMSM_P;               /* Mask Parameter: InteriorPMSM_P
                                        * Referenced by:
                                        *   '<S5>/Gain'
                                        *   '<S5>/Gain4'
                                        *   '<S18>/Gain2'
                                        */
  real_T InteriorPMSM_Rs;              /* Mask Parameter: InteriorPMSM_Rs
                                        * Referenced by:
                                        *   '<S6>/Constant'
                                        *   '<S26>/Constant'
                                        *   '<S30>/Constant'
                                        */
  real_T Int_Ts;                       /* Mask Parameter: Int_Ts
                                        * Referenced by: '<S8>/Gain1'
                                        */
  real_T Int_U;                        /* Mask Parameter: Int_U
                                        * Referenced by: '<S9>/Constant'
                                        */
  real_T InteriorPMSM_idq0[2];         /* Mask Parameter: InteriorPMSM_idq0
                                        * Referenced by:
                                        *   '<S22>/Constant'
                                        *   '<S23>/Constant'
                                        */
  real_T InteriorPMSM_lambda_pm_calc;
                                  /* Mask Parameter: InteriorPMSM_lambda_pm_calc
                                   * Referenced by:
                                   *   '<S18>/Gain1'
                                   *   '<S29>/Constant'
                                   */
  real_T InteriorPMSM_theta_init;     /* Mask Parameter: InteriorPMSM_theta_init
                                       * Referenced by: '<S8>/Unit Delay'
                                       */
  real_T Constant_Value;               /* Expression: 0
                                        * Referenced by: '<S1>/Constant'
                                        */
  real_T Constant1_Value;              /* Expression: 1
                                        * Referenced by: '<S22>/Constant1'
                                        */
  real_T DiscreteTimeIntegrator3_gainval;
                          /* Computed Parameter: DiscreteTimeIntegrator3_gainval
                           * Referenced by: '<S22>/Discrete-Time Integrator3'
                           */
  real_T DiscreteTimeIntegrator3_UpperSa;/* Expression: 10e6
                                          * Referenced by: '<S22>/Discrete-Time Integrator3'
                                          */
  real_T DiscreteTimeIntegrator3_LowerSa;/* Expression: -10e6
                                          * Referenced by: '<S22>/Discrete-Time Integrator3'
                                          */
  real_T Constant1_Value_c;            /* Expression: 2
                                        * Referenced by: '<S23>/Constant1'
                                        */
  real_T DiscreteTimeIntegrator3_gainv_i;
                          /* Computed Parameter: DiscreteTimeIntegrator3_gainv_i
                           * Referenced by: '<S23>/Discrete-Time Integrator3'
                           */
  real_T DiscreteTimeIntegrator3_Upper_d;/* Expression: 10e6
                                          * Referenced by: '<S23>/Discrete-Time Integrator3'
                                          */
  real_T DiscreteTimeIntegrator3_Lower_d;/* Expression: -10e6
                                          * Referenced by: '<S23>/Discrete-Time Integrator3'
                                          */
  real_T Gain2_Gain;                   /* Expression: -1/2
                                        * Referenced by: '<S16>/Gain2'
                                        */
  real_T Gain3_Gain;                   /* Expression: sqrt(3)/2
                                        * Referenced by: '<S16>/Gain3'
                                        */
  real_T Gain1_Gain;                   /* Expression: -1/2
                                        * Referenced by: '<S16>/Gain1'
                                        */
  real_T Gain4_Gain;                   /* Expression: -sqrt(3)/2
                                        * Referenced by: '<S16>/Gain4'
                                        */
  real_T Saturation2_UpperSat;         /* Expression: 1
                                        * Referenced by: '<S1>/Saturation2'
                                        */
  real_T Saturation2_LowerSat;         /* Expression: 0
                                        * Referenced by: '<S1>/Saturation2'
                                        */
  real_T Gain_Gain;                    /* Expression: 1/3
                                        * Referenced by: '<S1>/Gain'
                                        */
  real_T Switch_Threshold;             /* Expression: 0
                                        * Referenced by: '<S1>/Switch'
                                        */
  real_T Gain_Gain_k;                  /* Expression: 2/3
                                        * Referenced by: '<S15>/Gain'
                                        */
  real_T Gain1_Gain_o;                 /* Expression: -1/3
                                        * Referenced by: '<S15>/Gain1'
                                        */
  real_T Gain4_Gain_n;                 /* Expression: -1/3
                                        * Referenced by: '<S15>/Gain4'
                                        */
  real_T Gain2_Gain_k;                 /* Expression: sqrt(3)/3
                                        * Referenced by: '<S15>/Gain2'
                                        */
  real_T Gain3_Gain_i;                 /* Expression: -sqrt(3)/3
                                        * Referenced by: '<S15>/Gain3'
                                        */
  real_T Constant1_Value_l;            /* Expression: 1
                                        * Referenced by: '<S21>/Constant1'
                                        */
  real_T Constant2_Value;              /* Expression: 2
                                        * Referenced by: '<S21>/Constant2'
                                        */
  real_T Gain_Gain_f;                  /* Expression: -1
                                        * Referenced by: '<S6>/Gain'
                                        */
  real_T Gain1_Gain_n;                 /* Expression: -1
                                        * Referenced by: '<S6>/Gain1'
                                        */
  real_T Constant_Value_i;             /* Expression: 0
                                        * Referenced by: '<S5>/Constant'
                                        */
  real_T Gain2_Gain_l;                 /* Expression: -1
                                        * Referenced by: '<S6>/Gain2'
                                        */
  real_T Constant1_Value_k;            /* Expression: 1
                                        * Referenced by: '<S24>/Constant1'
                                        */
  real_T Constant1_Value_i;            /* Expression: 2
                                        * Referenced by: '<S25>/Constant1'
                                        */
  real_T Constant2_Value_l;            /* Expression: 1
                                        * Referenced by: '<S25>/Constant2'
                                        */
  real_T Constant2_Value_i;            /* Expression: 1
                                        * Referenced by: '<S26>/Constant2'
                                        */
  real_T Constant1_Value_m;            /* Expression: 2
                                        * Referenced by: '<S27>/Constant1'
                                        */
  real_T Constant1_Value_d;            /* Expression: 1
                                        * Referenced by: '<S28>/Constant1'
                                        */
  real_T Constant2_Value_o;            /* Expression: 2
                                        * Referenced by: '<S28>/Constant2'
                                        */
  real_T Constant2_Value_d;            /* Expression: 2
                                        * Referenced by: '<S29>/Constant2'
                                        */
  real_T Constant2_Value_dv;           /* Expression: 2
                                        * Referenced by: '<S30>/Constant2'
                                        */
  real32_T Constant1_Value_cx;         /* Computed Parameter: Constant1_Value_cx
                                        * Referenced by: '<S8>/Constant1'
                                        */
  real32_T Constant3_Value;            /* Computed Parameter: Constant3_Value
                                        * Referenced by: '<S8>/Constant3'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_motor_T {
  const char_T *errorStatus;
  RTWLogInfo *rtwLogInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    uint32_T clockTickH0;
    time_T stepSize0;
    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block parameters (default storage) */
extern P_motor_T motor_P;

/* Block signals (default storage) */
extern B_motor_T motor_B;

/* Block states (default storage) */
extern DW_motor_T motor_DW;

/* External inputs (root inport signals with default storage) */
extern ExtU_motor_T motor_U;

/* External outputs (root outports fed by signals with default storage) */
extern ExtY_motor_T motor_Y;

/* Model entry point functions */
extern void motor_initialize(void);
extern void motor_step(void);
extern void motor_terminate(void);

/* Real-time Model object */
extern RT_MODEL_motor_T *const motor_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'motor'
 * '<S1>'   : 'motor/Average-Value Inverter'
 * '<S2>'   : 'motor/Interior PMSM'
 * '<S3>'   : 'motor/Interior PMSM/PMSM Speed Input Discrete'
 * '<S4>'   : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete'
 * '<S5>'   : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/Mechanical and Angle'
 * '<S6>'   : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/Motor Units1'
 * '<S7>'   : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/PMSM Electromagnetic'
 * '<S8>'   : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/Mechanical and Angle/Int'
 * '<S9>'   : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/Mechanical and Angle/Int/Compare To Constant'
 * '<S10>'  : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/Mechanical and Angle/Int/Compare To Constant1'
 * '<S11>'  : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/Motor Units1/Power Accounting Bus Creator'
 * '<S12>'  : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/Motor Units1/Power Accounting Bus Creator/PwrNotTrnsfrd Input'
 * '<S13>'  : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/Motor Units1/Power Accounting Bus Creator/PwrStored Input'
 * '<S14>'  : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/Motor Units1/Power Accounting Bus Creator/PwrTrnsfrd Input'
 * '<S15>'  : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/PMSM Electromagnetic/Clarke Transform'
 * '<S16>'  : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/PMSM Electromagnetic/Inverse Clarke Transform'
 * '<S17>'  : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/PMSM Electromagnetic/Inverse Park Transform'
 * '<S18>'  : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/PMSM Electromagnetic/Machine Torque'
 * '<S19>'  : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/PMSM Electromagnetic/PMSM Equivalent Circuit'
 * '<S20>'  : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/PMSM Electromagnetic/Park Transform'
 * '<S21>'  : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/PMSM Electromagnetic/Machine Torque/Subsystem'
 * '<S22>'  : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage'
 * '<S23>'  : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage1'
 * '<S24>'  : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage/Subsystem'
 * '<S25>'  : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage/Subsystem1'
 * '<S26>'  : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage/Subsystem2'
 * '<S27>'  : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage1/Subsystem'
 * '<S28>'  : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage1/Subsystem1'
 * '<S29>'  : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage1/Subsystem2'
 * '<S30>'  : 'motor/Interior PMSM/PMSM Speed Input Discrete/PMSM Speed Input Core Discrete/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage1/Subsystem3'
 */
#endif                                 /* RTW_HEADER_motor_h_ */
