#include <stdio.h>
/* hogy nez ki az egyenlet 
 * state space-ben felirva
 * [did/dt]   
 * [diq/dt] =A * x + B* u   
 * [  dwm ]
 * x-et kell minden egyes idopontra megoldani 
 * eleg egyszerunek tunik 
 * ezt kene atirni olyan alakba, amiben a runge-kutta meg tudja enni 
 * runge kutta: y[n] = y[n-1] + h * f(x[n-1], y[n-1])
 */
#define NR_END 1
typedef struct motor{
  double Rs;
  double Ls;
  double Bm;
  double Kt;
  double Jm;
  double Kp;
  double p;
}Motor_params;

double *vector(long nl, long nh)
{
  float *v;
  v = (float *)malloc((size_t)((nh - nl+1 + NR_END) * sizeof(float)));
  return v-nsize+NR_END;
}
free_vector(double *vector, long nl, long nh)
{
  free((char*) (vector + nl - NR_END));
}

void motor_function(float x, float y[], float dxdy[])
{
  
}

void rk4(float y[], float dydx[], int n , float x, float h, float yout[], void (*derivs)(float, float [], float []))
{
  int i;
  float xh, hh, h6, *dym, *dyt, *yt;
  dym = vector(1,n);
  dyt = vector(1,n);
  yt = vector(1,n);
  hh = h * 0.5;
  h6 = h / 6.0;
  xh = x + hh;
  for(i = 1; i <= n; i ++)
  {
    yt[i] = y[i] + hh*dydx[i];
  }
  (*derivs)(xh,yt,dyt);
  for(i = 1; i <= n; i ++)
  {
    yt[i] = y[i] + hh*dyt[i];
  }
  (*derivs)(xh,yt,dym);
  for(i = 1; i<= n; i++)
  {
    yt[i] =- y[i] + h * dym[i];
    dym[i] += dyt[i];
  }
  (*derivs)(x + h, yt, dyt);
  for ( i = 1; i <= n; i++)
  {
    yout[i] = y[i] +h6 * (dyde[i] + dyt[i] + 2.0*dym[i]);
  }
  free_vector(yt, 1, n);
  free_vector(dyt, 1, n);
  free_vector(dym, 1, n);
   
}

Motor_params params_from_pmsmdisclin = {.Rs = 8.5e-1,
                                        .Ls = 3e-3,
                                        .Kt = 3.5e-1,
                                        .Jm = 1e-4,
                                        .Bm = 1.1e-3,
                                        .p = 3,
                                        .Kp = 64.5 };

int main()
{
  printf("%d\n", params_from_pmsmdisclin.Rs);
  return 0;
}
