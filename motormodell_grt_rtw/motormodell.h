/*
 * motormodell.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "motormodell".
 *
 * Model version              : 1.2
 * Simulink Coder version : 9.3 (R2020a) 18-Nov-2019
 * C source code generated on : Tue Feb 23 13:09:57 2021
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Emulation hardware selection:
 *    Differs from embedded hardware (MATLAB Host)
 * Code generation objective: Execution efficiency
 * Validation result: Passed (3), Warning (1), Error (0)
 */

#ifndef RTW_HEADER_motormodell_h_
#define RTW_HEADER_motormodell_h_
#include <string.h>
#include <math.h>
#include <float.h>
#include <stddef.h>
#ifndef motormodell_COMMON_INCLUDES_
# define motormodell_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "rt_logging.h"
#endif                                 /* motormodell_COMMON_INCLUDES_ */

#include "motormodell_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rt_nonfinite.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetContStateDisabled
# define rtmGetContStateDisabled(rtm)  ((rtm)->contStateDisabled)
#endif

#ifndef rtmSetContStateDisabled
# define rtmSetContStateDisabled(rtm, val) ((rtm)->contStateDisabled = (val))
#endif

#ifndef rtmGetContStates
# define rtmGetContStates(rtm)         ((rtm)->contStates)
#endif

#ifndef rtmSetContStates
# define rtmSetContStates(rtm, val)    ((rtm)->contStates = (val))
#endif

#ifndef rtmGetContTimeOutputInconsistentWithStateAtMajorStepFlag
# define rtmGetContTimeOutputInconsistentWithStateAtMajorStepFlag(rtm) ((rtm)->CTOutputIncnstWithState)
#endif

#ifndef rtmSetContTimeOutputInconsistentWithStateAtMajorStepFlag
# define rtmSetContTimeOutputInconsistentWithStateAtMajorStepFlag(rtm, val) ((rtm)->CTOutputIncnstWithState = (val))
#endif

#ifndef rtmGetDerivCacheNeedsReset
# define rtmGetDerivCacheNeedsReset(rtm) ((rtm)->derivCacheNeedsReset)
#endif

#ifndef rtmSetDerivCacheNeedsReset
# define rtmSetDerivCacheNeedsReset(rtm, val) ((rtm)->derivCacheNeedsReset = (val))
#endif

#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetIntgData
# define rtmGetIntgData(rtm)           ((rtm)->intgData)
#endif

#ifndef rtmSetIntgData
# define rtmSetIntgData(rtm, val)      ((rtm)->intgData = (val))
#endif

#ifndef rtmGetOdeF
# define rtmGetOdeF(rtm)               ((rtm)->odeF)
#endif

#ifndef rtmSetOdeF
# define rtmSetOdeF(rtm, val)          ((rtm)->odeF = (val))
#endif

#ifndef rtmGetOdeY
# define rtmGetOdeY(rtm)               ((rtm)->odeY)
#endif

#ifndef rtmSetOdeY
# define rtmSetOdeY(rtm, val)          ((rtm)->odeY = (val))
#endif

#ifndef rtmGetPeriodicContStateIndices
# define rtmGetPeriodicContStateIndices(rtm) ((rtm)->periodicContStateIndices)
#endif

#ifndef rtmSetPeriodicContStateIndices
# define rtmSetPeriodicContStateIndices(rtm, val) ((rtm)->periodicContStateIndices = (val))
#endif

#ifndef rtmGetPeriodicContStateRanges
# define rtmGetPeriodicContStateRanges(rtm) ((rtm)->periodicContStateRanges)
#endif

#ifndef rtmSetPeriodicContStateRanges
# define rtmSetPeriodicContStateRanges(rtm, val) ((rtm)->periodicContStateRanges = (val))
#endif

#ifndef rtmGetRTWLogInfo
# define rtmGetRTWLogInfo(rtm)         ((rtm)->rtwLogInfo)
#endif

#ifndef rtmGetZCCacheNeedsReset
# define rtmGetZCCacheNeedsReset(rtm)  ((rtm)->zCCacheNeedsReset)
#endif

#ifndef rtmSetZCCacheNeedsReset
# define rtmSetZCCacheNeedsReset(rtm, val) ((rtm)->zCCacheNeedsReset = (val))
#endif

#ifndef rtmGetdX
# define rtmGetdX(rtm)                 ((rtm)->derivs)
#endif

#ifndef rtmSetdX
# define rtmSetdX(rtm, val)            ((rtm)->derivs = (val))
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  (rtmGetTPtr((rtm))[0])
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetTPtr
# define rtmGetTPtr(rtm)               ((rtm)->Timing.t)
#endif

/* Block signals (default storage) */
typedef struct {
  real_T Int;                          /* '<S4>/Int' */
  real_T Add;                          /* '<S19>/Add' */
  real_T Add_i;                        /* '<S18>/Add' */
  real_T Divide;                       /* '<S4>/Divide' */
} B_motormodell_T;

/* Block states (default storage) for system '<Root>' */
typedef struct {
  int_T Integrator_IWORK;              /* '<S19>/Integrator' */
  int_T Integrator_IWORK_n;            /* '<S18>/Integrator' */
} DW_motormodell_T;

/* Continuous states (default storage) */
typedef struct {
  real_T Integrator_CSTATE;            /* '<S19>/Integrator' */
  real_T Int1_CSTATE;                  /* '<S4>/Int1' */
  real_T Integrator_CSTATE_o;          /* '<S18>/Integrator' */
  real_T Int_CSTATE;                   /* '<S4>/Int' */
} X_motormodell_T;

/* Periodic continuous state vector (global) */
typedef int_T PeriodicIndX_motormodell_T[1];
typedef real_T PeriodicRngX_motormodell_T[2];

/* State derivatives (default storage) */
typedef struct {
  real_T Integrator_CSTATE;            /* '<S19>/Integrator' */
  real_T Int1_CSTATE;                  /* '<S4>/Int1' */
  real_T Integrator_CSTATE_o;          /* '<S18>/Integrator' */
  real_T Int_CSTATE;                   /* '<S4>/Int' */
} XDot_motormodell_T;

/* State disabled  */
typedef struct {
  boolean_T Integrator_CSTATE;         /* '<S19>/Integrator' */
  boolean_T Int1_CSTATE;               /* '<S4>/Int1' */
  boolean_T Integrator_CSTATE_o;       /* '<S18>/Integrator' */
  boolean_T Int_CSTATE;                /* '<S4>/Int' */
} XDis_motormodell_T;

#ifndef ODE3_INTG
#define ODE3_INTG

/* ODE3 Integration Data */
typedef struct {
  real_T *y;                           /* output */
  real_T *f[3];                        /* derivatives */
} ODE3_IntgData;

#endif

/* External inputs (root inport signals with default storage) */
typedef struct {
  real_T LoadTorque;                   /* '<Root>/LoadTorque' */
  real_T PhaseVoltages[3];             /* '<Root>/PhaseVoltages' */
} ExtU_motormodell_T;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  real_T info[15];                     /* '<Root>/info' */
  real_T phase_curr[3];                /* '<Root>/phase_curr' */
  real_T mot_speed;                    /* '<Root>/mot_speed' */
} ExtY_motormodell_T;

/* Real-time Model Data Structure */
struct tag_RTM_motormodell_T {
  const char_T *errorStatus;
  RTWLogInfo *rtwLogInfo;
  RTWSolverInfo solverInfo;
  X_motormodell_T *contStates;
  int_T *periodicContStateIndices;
  real_T *periodicContStateRanges;
  real_T *derivs;
  boolean_T *contStateDisabled;
  boolean_T zCCacheNeedsReset;
  boolean_T derivCacheNeedsReset;
  boolean_T CTOutputIncnstWithState;
  real_T odeY[4];
  real_T odeF[3][4];
  ODE3_IntgData intgData;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    int_T numContStates;
    int_T numPeriodicContStates;
    int_T numSampTimes;
  } Sizes;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    uint32_T clockTick0;
    uint32_T clockTickH0;
    time_T stepSize0;
    uint32_T clockTick1;
    uint32_T clockTickH1;
    boolean_T firstInitCondFlag;
    time_T tFinal;
    SimTimeStep simTimeStep;
    boolean_T stopRequestedFlag;
    time_T *t;
    time_T tArray[2];
  } Timing;
};

/* Block signals (default storage) */
extern B_motormodell_T motormodell_B;

/* Continuous states (default storage) */
extern X_motormodell_T motormodell_X;

/* Block states (default storage) */
extern DW_motormodell_T motormodell_DW;

/* External inputs (root inport signals with default storage) */
extern ExtU_motormodell_T motormodell_U;

/* External outputs (root outports fed by signals with default storage) */
extern ExtY_motormodell_T motormodell_Y;

/* Model entry point functions */
extern void motormodell_initialize(void);
extern void motormodell_step(void);
extern void motormodell_terminate(void);

/* Real-time Model object */
extern RT_MODEL_motormodell_T *const motormodell_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'motormodell'
 * '<S1>'   : 'motormodell/Interior PMSM'
 * '<S2>'   : 'motormodell/Interior PMSM/PMSM Torque Input Continuous'
 * '<S3>'   : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core'
 * '<S4>'   : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/Mechanical and Angle'
 * '<S5>'   : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/Motor Units1'
 * '<S6>'   : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic'
 * '<S7>'   : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/Motor Units1/Power Accounting Bus Creator'
 * '<S8>'   : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/Motor Units1/Power Accounting Bus Creator/PwrNotTrnsfrd Input'
 * '<S9>'   : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/Motor Units1/Power Accounting Bus Creator/PwrStored Input'
 * '<S10>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/Motor Units1/Power Accounting Bus Creator/PwrTrnsfrd Input'
 * '<S11>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/Clarke Transform'
 * '<S12>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/Inverse Clarke Transform'
 * '<S13>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/Inverse Park Transform'
 * '<S14>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/Machine Torque'
 * '<S15>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/PMSM Equivalent Circuit'
 * '<S16>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/Park Transform'
 * '<S17>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/Machine Torque/Subsystem'
 * '<S18>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage'
 * '<S19>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage1'
 * '<S20>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage/Subsystem'
 * '<S21>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage/Subsystem1'
 * '<S22>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage/Subsystem2'
 * '<S23>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage1/Subsystem'
 * '<S24>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage1/Subsystem1'
 * '<S25>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage1/Subsystem2'
 * '<S26>'  : 'motormodell/Interior PMSM/PMSM Torque Input Continuous/PMSM Torque Input Core/PMSM Electromagnetic/PMSM Equivalent Circuit/D Axis Stator Voltage1/Subsystem3'
 */
#endif                                 /* RTW_HEADER_motormodell_h_ */
