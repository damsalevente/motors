/*
 * motormodell.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "motormodell".
 *
 * Model version              : 1.2
 * Simulink Coder version : 9.3 (R2020a) 18-Nov-2019
 * C source code generated on : Tue Feb 23 13:09:57 2021
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Emulation hardware selection:
 *    Differs from embedded hardware (MATLAB Host)
 * Code generation objective: Execution efficiency
 * Validation result: Passed (3), Warning (1), Error (0)
 */

#include "motormodell.h"
#include "motormodell_private.h"

/* Block signals (default storage) */
B_motormodell_T motormodell_B;

/* Continuous states */
X_motormodell_T motormodell_X;

/* Periodic continuous states */
PeriodicIndX_motormodell_T motormodell_PeriodicIndX;
PeriodicRngX_motormodell_T motormodell_PeriodicRngX;

/* Block states (default storage) */
DW_motormodell_T motormodell_DW;

/* External inputs (root inport signals with default storage) */
ExtU_motormodell_T motormodell_U;

/* External outputs (root outports fed by signals with default storage) */
ExtY_motormodell_T motormodell_Y;

/* Real-time model */
RT_MODEL_motormodell_T motormodell_M_;
RT_MODEL_motormodell_T *const motormodell_M = &motormodell_M_;

/* State reduction function */
void local_stateReduction(real_T* x, int_T* p, int_T n, real_T* r)
{
  int_T i, j;
  for (i = 0, j = 0; i < n; ++i, ++j) {
    int_T k = p[i];
    real_T lb = r[j++];
    real_T xk = x[k]-lb;
    real_T rk = r[j]-lb;
    int_T q = (int_T) floor(xk/rk);
    if (q) {
      x[k] = xk-q*rk+lb;
    }
  }
}

/*
 * This function updates continuous states using the ODE3 fixed-step
 * solver algorithm
 */
static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si )
{
  /* Solver Matrices */
  static const real_T rt_ODE3_A[3] = {
    1.0/2.0, 3.0/4.0, 1.0
  };

  static const real_T rt_ODE3_B[3][3] = {
    { 1.0/2.0, 0.0, 0.0 },

    { 0.0, 3.0/4.0, 0.0 },

    { 2.0/9.0, 1.0/3.0, 4.0/9.0 }
  };

  time_T t = rtsiGetT(si);
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE3_IntgData *id = (ODE3_IntgData *)rtsiGetSolverData(si);
  real_T *y = id->y;
  real_T *f0 = id->f[0];
  real_T *f1 = id->f[1];
  real_T *f2 = id->f[2];
  real_T hB[3];
  int_T i;
  int_T nXc = 4;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);

  /* Save the state values at time t in y, we'll use x as ynew. */
  (void) memcpy(y, x,
                (uint_T)nXc*sizeof(real_T));

  /* Assumes that rtsiSetT and ModelOutputs are up-to-date */
  /* f0 = f(t,y) */
  rtsiSetdX(si, f0);
  motormodell_derivatives();

  /* f(:,2) = feval(odefile, t + hA(1), y + f*hB(:,1), args(:)(*)); */
  hB[0] = h * rt_ODE3_B[0][0];
  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0]);
  }

  rtsiSetT(si, t + h*rt_ODE3_A[0]);
  rtsiSetdX(si, f1);
  motormodell_step();
  motormodell_derivatives();

  /* f(:,3) = feval(odefile, t + hA(2), y + f*hB(:,2), args(:)(*)); */
  for (i = 0; i <= 1; i++) {
    hB[i] = h * rt_ODE3_B[1][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1]);
  }

  rtsiSetT(si, t + h*rt_ODE3_A[1]);
  rtsiSetdX(si, f2);
  motormodell_step();
  motormodell_derivatives();

  /* tnew = t + hA(3);
     ynew = y + f*hB(:,3); */
  for (i = 0; i <= 2; i++) {
    hB[i] = h * rt_ODE3_B[2][i];
  }

  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (f0[i]*hB[0] + f1[i]*hB[1] + f2[i]*hB[2]);
  }

  rtsiSetT(si, tnew);
  local_stateReduction(x, rtsiGetPeriodicContStateIndices(si), 1,
                       rtsiGetPeriodicContStateRanges(si));
  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

/* Model step function */
void motormodell_step(void)
{
  real_T rtb_Integrator;
  real_T rtb_Product;
  real_T rtb_Subtract2;
  real_T rtb_Gain2_f;
  real_T rtb_Add1_a;
  real_T rtb_Add_l;
  real_T rtb_Integrator_b;
  real_T rtb_Add1;
  real_T rtb_Add_m;
  real_T rtb_Gain2_l;
  real_T rtb_Gain1_p;
  real_T rtb_Int1;
  real_T rtb_Gain4;
  if (rtmIsMajorTimeStep(motormodell_M)) {
    /* set solver stop time */
    if (!(motormodell_M->Timing.clockTick0+1)) {
      rtsiSetSolverStopTime(&motormodell_M->solverInfo,
                            ((motormodell_M->Timing.clockTickH0 + 1) *
        motormodell_M->Timing.stepSize0 * 4294967296.0));
    } else {
      rtsiSetSolverStopTime(&motormodell_M->solverInfo,
                            ((motormodell_M->Timing.clockTick0 + 1) *
        motormodell_M->Timing.stepSize0 + motormodell_M->Timing.clockTickH0 *
        motormodell_M->Timing.stepSize0 * 4294967296.0));
    }
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep(motormodell_M)) {
    motormodell_M->Timing.t[0] = rtsiGetT(&motormodell_M->solverInfo);
  }

  /* Integrator: '<S19>/Integrator' */
  /* Limited  Integrator  */
  if (motormodell_DW.Integrator_IWORK != 0) {
    motormodell_X.Integrator_CSTATE = 0.0;
  }

  if (motormodell_X.Integrator_CSTATE >= 1.0E+7) {
    if (motormodell_X.Integrator_CSTATE > 1.0E+7) {
      rtsiSetBlockStateForSolverChangedAtMajorStep(&motormodell_M->solverInfo,
        true);
    }

    motormodell_X.Integrator_CSTATE = 1.0E+7;
  } else {
    if (motormodell_X.Integrator_CSTATE <= -1.0E+7) {
      if (motormodell_X.Integrator_CSTATE < -1.0E+7) {
        rtsiSetBlockStateForSolverChangedAtMajorStep(&motormodell_M->solverInfo,
          true);
      }

      motormodell_X.Integrator_CSTATE = -1.0E+7;
    }
  }

  rtb_Integrator = motormodell_X.Integrator_CSTATE;

  /* Product: '<S26>/Product' incorporates:
   *  Constant: '<S26>/Constant'
   *  Integrator: '<S19>/Integrator'
   */
  rtb_Product = motormodell_X.Integrator_CSTATE * 0.03 / 0.0032;

  /* Integrator: '<S4>/Int1' */
  rtb_Int1 = motormodell_X.Int1_CSTATE;

  /* Gain: '<S4>/Gain4' incorporates:
   *  Integrator: '<S4>/Int1'
   */
  rtb_Gain4 = 4.0 * motormodell_X.Int1_CSTATE;

  /* Trigonometry: '<S6>/sine_cosine' */
  rtb_Gain2_f = sin(rtb_Gain4);
  rtb_Gain4 = cos(rtb_Gain4);

  /* Sum: '<S11>/Add' incorporates:
   *  Gain: '<S11>/Gain'
   *  Gain: '<S11>/Gain1'
   *  Gain: '<S11>/Gain4'
   *  Inport: '<Root>/PhaseVoltages'
   */
  rtb_Subtract2 = (0.66666666666666663 * motormodell_U.PhaseVoltages[0] +
                   -0.33333333333333331 * motormodell_U.PhaseVoltages[1]) +
    -0.33333333333333331 * motormodell_U.PhaseVoltages[2];

  /* Sum: '<S11>/Add1' incorporates:
   *  Gain: '<S11>/Gain2'
   *  Gain: '<S11>/Gain3'
   *  Inport: '<Root>/PhaseVoltages'
   */
  rtb_Add1_a = 0.57735026918962573 * motormodell_U.PhaseVoltages[1] +
    -0.57735026918962573 * motormodell_U.PhaseVoltages[2];

  /* Sum: '<S16>/Add' incorporates:
   *  Product: '<S16>/Product'
   *  Product: '<S16>/Product1'
   */
  rtb_Add_l = rtb_Add1_a * rtb_Gain4 - rtb_Subtract2 * rtb_Gain2_f;

  /* Integrator: '<S18>/Integrator' */
  /* Limited  Integrator  */
  if (motormodell_DW.Integrator_IWORK_n != 0) {
    motormodell_X.Integrator_CSTATE_o = 0.0;
  }

  if (motormodell_X.Integrator_CSTATE_o >= 1.0E+7) {
    if (motormodell_X.Integrator_CSTATE_o > 1.0E+7) {
      rtsiSetBlockStateForSolverChangedAtMajorStep(&motormodell_M->solverInfo,
        true);
    }

    motormodell_X.Integrator_CSTATE_o = 1.0E+7;
  } else {
    if (motormodell_X.Integrator_CSTATE_o <= -1.0E+7) {
      if (motormodell_X.Integrator_CSTATE_o < -1.0E+7) {
        rtsiSetBlockStateForSolverChangedAtMajorStep(&motormodell_M->solverInfo,
          true);
      }

      motormodell_X.Integrator_CSTATE_o = -1.0E+7;
    }
  }

  rtb_Integrator_b = motormodell_X.Integrator_CSTATE_o;

  /* End of Integrator: '<S18>/Integrator' */

  /* Integrator: '<S4>/Int' */
  /* Limited  Integrator  */
  if (motormodell_X.Int_CSTATE >= 1.0E+7) {
    if (motormodell_X.Int_CSTATE > 1.0E+7) {
      rtsiSetBlockStateForSolverChangedAtMajorStep(&motormodell_M->solverInfo,
        true);
    }

    motormodell_X.Int_CSTATE = 1.0E+7;
  } else {
    if (motormodell_X.Int_CSTATE <= -1.0E+7) {
      if (motormodell_X.Int_CSTATE < -1.0E+7) {
        rtsiSetBlockStateForSolverChangedAtMajorStep(&motormodell_M->solverInfo,
          true);
      }

      motormodell_X.Int_CSTATE = -1.0E+7;
    }
  }

  motormodell_B.Int = motormodell_X.Int_CSTATE;

  /* End of Integrator: '<S4>/Int' */

  /* Gain: '<S4>/Gain' */
  rtb_Add1 = 4.0 * motormodell_B.Int;

  /* Sum: '<S19>/Add' incorporates:
   *  Constant: '<S25>/Constant'
   *  Product: '<S19>/Product'
   *  Product: '<S23>/Product'
   *  Product: '<S24>/Product'
   *  Product: '<S25>/Product'
   */
  motormodell_B.Add = ((rtb_Add_l / 0.0032 - rtb_Integrator_b * rtb_Add1 *
                        0.0013 / 0.0032) - rtb_Add1 * 0.042 / 0.0032) -
    rtb_Product;

  /* Product: '<S18>/Product' */
  rtb_Product = rtb_Add1 * rtb_Integrator;

  /* Sum: '<S16>/Add1' incorporates:
   *  Product: '<S16>/Product2'
   *  Product: '<S16>/Product3'
   */
  rtb_Add1 = rtb_Subtract2 * rtb_Gain4 + rtb_Add1_a * rtb_Gain2_f;

  /* Sum: '<S18>/Add' incorporates:
   *  Constant: '<S22>/Constant'
   *  Product: '<S20>/Product'
   *  Product: '<S21>/Product'
   *  Product: '<S22>/Product'
   */
  motormodell_B.Add_i = (rtb_Product * 0.0032 / 0.0013 + rtb_Add1 / 0.0013) -
    rtb_Integrator_b * 0.03 / 0.0013;

  /* Sum: '<S13>/Add1' incorporates:
   *  Product: '<S13>/Product2'
   *  Product: '<S13>/Product3'
   */
  rtb_Add1_a = rtb_Integrator_b * rtb_Gain4 - rtb_Integrator * rtb_Gain2_f;

  /* Sum: '<S13>/Add' incorporates:
   *  Product: '<S13>/Product'
   *  Product: '<S13>/Product1'
   */
  rtb_Subtract2 = rtb_Integrator_b * rtb_Gain2_f + rtb_Integrator * rtb_Gain4;

  /* Sum: '<S12>/Subtract1' incorporates:
   *  Gain: '<S12>/Gain2'
   *  Gain: '<S12>/Gain3'
   */
  rtb_Gain4 = -0.5 * rtb_Add1_a + 0.8660254037844386 * rtb_Subtract2;

  /* Sum: '<S12>/Subtract2' incorporates:
   *  Gain: '<S12>/Gain1'
   *  Gain: '<S12>/Gain4'
   */
  rtb_Subtract2 = -0.5 * rtb_Add1_a + -0.8660254037844386 * rtb_Subtract2;

  /* Gain: '<S14>/Gain2' incorporates:
   *  Gain: '<S14>/Gain1'
   *  Product: '<S14>/Product'
   *  Product: '<S17>/Product'
   *  Sum: '<S14>/Add'
   */
  rtb_Gain2_f = (rtb_Integrator_b * rtb_Integrator * -0.0019000000000000002 +
                 0.042 * rtb_Integrator) * 6.0;

  /* Gain: '<S5>/Gain' incorporates:
   *  Product: '<S5>/Product3'
   */
  rtb_Product = -(motormodell_B.Int * rtb_Gain2_f);

  /* Sum: '<S5>/Add' incorporates:
   *  Inport: '<Root>/PhaseVoltages'
   *  Product: '<S5>/Product'
   *  Product: '<S5>/Product1'
   *  Product: '<S5>/Product2'
   */
  rtb_Add_m = (rtb_Add1_a * motormodell_U.PhaseVoltages[0] + rtb_Gain4 *
               motormodell_U.PhaseVoltages[1]) + rtb_Subtract2 *
    motormodell_U.PhaseVoltages[2];

  /* Gain: '<S5>/Gain1' incorporates:
   *  Constant: '<S5>/Constant'
   *  Product: '<S5>/Product11'
   *  Product: '<S5>/Product12'
   *  Product: '<S5>/Product4'
   *  Sum: '<S5>/Add2'
   */
  rtb_Gain1_p = -((rtb_Integrator_b * rtb_Integrator_b + rtb_Integrator *
                   rtb_Integrator) * 0.03);

  /* Gain: '<S5>/Gain2' incorporates:
   *  Abs: '<S4>/Abs'
   *  Product: '<S4>/Product3'
   *  Product: '<S4>/Product4'
   *  Sum: '<S4>/Add'
   */
  rtb_Gain2_l = -(motormodell_B.Int * motormodell_B.Int * 0.004924 + fabs
                  (motormodell_B.Int) * 0.0);

  /* Outport: '<Root>/info' incorporates:
   *  Sum: '<S5>/Add4'
   */
  motormodell_Y.info[0] = rtb_Add1_a;
  motormodell_Y.info[1] = rtb_Gain4;
  motormodell_Y.info[2] = rtb_Subtract2;
  motormodell_Y.info[3] = rtb_Integrator_b;
  motormodell_Y.info[4] = rtb_Integrator;
  motormodell_Y.info[5] = rtb_Add1;
  motormodell_Y.info[6] = rtb_Add_l;
  motormodell_Y.info[7] = motormodell_B.Int;
  motormodell_Y.info[8] = rtb_Int1;
  motormodell_Y.info[9] = rtb_Gain2_f;
  motormodell_Y.info[10] = rtb_Product;
  motormodell_Y.info[11] = rtb_Add_m;
  motormodell_Y.info[12] = rtb_Gain1_p;
  motormodell_Y.info[13] = rtb_Gain2_l;
  motormodell_Y.info[14] = ((rtb_Product + rtb_Add_m) + rtb_Gain1_p) +
    rtb_Gain2_l;

  /* Product: '<S4>/Divide' incorporates:
   *  Gain: '<S4>/Gain5'
   *  Inport: '<Root>/LoadTorque'
   *  Product: '<S4>/Divide1'
   *  Product: '<S4>/Divide2'
   *  Sum: '<S4>/Sum1'
   *  Trigonometry: '<S4>/Trigonometric Function'
   */
  motormodell_B.Divide = (((rtb_Gain2_f - motormodell_U.LoadTorque) -
    motormodell_B.Int * 0.004924) - tanh(4.0 * motormodell_B.Int) * 0.0) /
    0.0334;

  /* SignalConversion generated from: '<S3>/Vector Concatenate' incorporates:
   *  Outport: '<Root>/phase_curr'
   */
  motormodell_Y.phase_curr[2] = rtb_Subtract2;

  /* SignalConversion generated from: '<S3>/Vector Concatenate' incorporates:
   *  Outport: '<Root>/phase_curr'
   */
  motormodell_Y.phase_curr[1] = rtb_Gain4;

  /* SignalConversion generated from: '<S3>/Vector Concatenate' incorporates:
   *  Outport: '<Root>/phase_curr'
   */
  motormodell_Y.phase_curr[0] = rtb_Add1_a;

  /* Outport: '<Root>/mot_speed' */
  motormodell_Y.mot_speed = motormodell_B.Int;
  if (rtmIsMajorTimeStep(motormodell_M)) {
    /* Matfile logging */
    rt_UpdateTXYLogVars(motormodell_M->rtwLogInfo, (motormodell_M->Timing.t));
  }                                    /* end MajorTimeStep */

  if (rtmIsMajorTimeStep(motormodell_M)) {
    /* Update for Integrator: '<S19>/Integrator' */
    motormodell_DW.Integrator_IWORK = 0;

    /* Update for Integrator: '<S18>/Integrator' */
    motormodell_DW.Integrator_IWORK_n = 0;
  }                                    /* end MajorTimeStep */

  if (rtmIsMajorTimeStep(motormodell_M)) {
    /* signal main to stop simulation */
    {                                  /* Sample time: [0.0s, 0.0s] */
      if ((rtmGetTFinal(motormodell_M)!=-1) &&
          !((rtmGetTFinal(motormodell_M)-(((motormodell_M->Timing.clockTick1+
               motormodell_M->Timing.clockTickH1* 4294967296.0)) * 0.2)) >
            (((motormodell_M->Timing.clockTick1+
               motormodell_M->Timing.clockTickH1* 4294967296.0)) * 0.2) *
            (DBL_EPSILON))) {
        rtmSetErrorStatus(motormodell_M, "Simulation finished");
      }
    }

    rt_ertODEUpdateContinuousStates(&motormodell_M->solverInfo);

    /* Update absolute time for base rate */
    /* The "clockTick0" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick0"
     * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick0 and the high bits
     * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++motormodell_M->Timing.clockTick0)) {
      ++motormodell_M->Timing.clockTickH0;
    }

    motormodell_M->Timing.t[0] = rtsiGetSolverStopTime
      (&motormodell_M->solverInfo);

    {
      /* Update absolute timer for sample time: [0.2s, 0.0s] */
      /* The "clockTick1" counts the number of times the code of this task has
       * been executed. The resolution of this integer timer is 0.2, which is the step size
       * of the task. Size of "clockTick1" ensures timer will not overflow during the
       * application lifespan selected.
       * Timer of this task consists of two 32 bit unsigned integers.
       * The two integers represent the low bits Timing.clockTick1 and the high bits
       * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
       */
      motormodell_M->Timing.clockTick1++;
      if (!motormodell_M->Timing.clockTick1) {
        motormodell_M->Timing.clockTickH1++;
      }
    }
  }                                    /* end MajorTimeStep */
}

/* Derivatives for root system: '<Root>' */
void motormodell_derivatives(void)
{
  boolean_T lsat;
  boolean_T usat;
  XDot_motormodell_T *_rtXdot;
  _rtXdot = ((XDot_motormodell_T *) motormodell_M->derivs);

  /* Derivatives for Integrator: '<S19>/Integrator' */
  lsat = (motormodell_X.Integrator_CSTATE <= -1.0E+7);
  usat = (motormodell_X.Integrator_CSTATE >= 1.0E+7);
  if (((!lsat) && (!usat)) || (lsat && (motormodell_B.Add > 0.0)) || (usat &&
       (motormodell_B.Add < 0.0))) {
    _rtXdot->Integrator_CSTATE = motormodell_B.Add;
  } else {
    /* in saturation */
    _rtXdot->Integrator_CSTATE = 0.0;
  }

  /* End of Derivatives for Integrator: '<S19>/Integrator' */

  /* Derivatives for Integrator: '<S4>/Int1' */
  _rtXdot->Int1_CSTATE = motormodell_B.Int;

  /* Derivatives for Integrator: '<S18>/Integrator' */
  lsat = (motormodell_X.Integrator_CSTATE_o <= -1.0E+7);
  usat = (motormodell_X.Integrator_CSTATE_o >= 1.0E+7);
  if (((!lsat) && (!usat)) || (lsat && (motormodell_B.Add_i > 0.0)) || (usat &&
       (motormodell_B.Add_i < 0.0))) {
    _rtXdot->Integrator_CSTATE_o = motormodell_B.Add_i;
  } else {
    /* in saturation */
    _rtXdot->Integrator_CSTATE_o = 0.0;
  }

  /* End of Derivatives for Integrator: '<S18>/Integrator' */

  /* Derivatives for Integrator: '<S4>/Int' */
  lsat = (motormodell_X.Int_CSTATE <= -1.0E+7);
  usat = (motormodell_X.Int_CSTATE >= 1.0E+7);
  if (((!lsat) && (!usat)) || (lsat && (motormodell_B.Divide > 0.0)) || (usat &&
       (motormodell_B.Divide < 0.0))) {
    _rtXdot->Int_CSTATE = motormodell_B.Divide;
  } else {
    /* in saturation */
    _rtXdot->Int_CSTATE = 0.0;
  }

  /* End of Derivatives for Integrator: '<S4>/Int' */
}

/* Model initialize function */
void motormodell_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)motormodell_M, 0,
                sizeof(RT_MODEL_motormodell_T));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&motormodell_M->solverInfo,
                          &motormodell_M->Timing.simTimeStep);
    rtsiSetTPtr(&motormodell_M->solverInfo, &rtmGetTPtr(motormodell_M));
    rtsiSetStepSizePtr(&motormodell_M->solverInfo,
                       &motormodell_M->Timing.stepSize0);
    rtsiSetdXPtr(&motormodell_M->solverInfo, &motormodell_M->derivs);
    rtsiSetContStatesPtr(&motormodell_M->solverInfo, (real_T **)
                         &motormodell_M->contStates);
    rtsiSetNumContStatesPtr(&motormodell_M->solverInfo,
      &motormodell_M->Sizes.numContStates);
    rtsiSetNumPeriodicContStatesPtr(&motormodell_M->solverInfo,
      &motormodell_M->Sizes.numPeriodicContStates);
    rtsiSetPeriodicContStateIndicesPtr(&motormodell_M->solverInfo,
      &motormodell_M->periodicContStateIndices);
    rtsiSetPeriodicContStateRangesPtr(&motormodell_M->solverInfo,
      &motormodell_M->periodicContStateRanges);
    rtsiSetErrorStatusPtr(&motormodell_M->solverInfo, (&rtmGetErrorStatus
      (motormodell_M)));
    rtsiSetRTModelPtr(&motormodell_M->solverInfo, motormodell_M);
  }

  rtsiSetSimTimeStep(&motormodell_M->solverInfo, MAJOR_TIME_STEP);
  motormodell_M->intgData.y = motormodell_M->odeY;
  motormodell_M->intgData.f[0] = motormodell_M->odeF[0];
  motormodell_M->intgData.f[1] = motormodell_M->odeF[1];
  motormodell_M->intgData.f[2] = motormodell_M->odeF[2];
  motormodell_M->contStates = ((X_motormodell_T *) &motormodell_X);
  motormodell_M->periodicContStateIndices = ((int_T*) motormodell_PeriodicIndX);
  motormodell_M->periodicContStateRanges = ((real_T*) motormodell_PeriodicRngX);
  rtsiSetSolverData(&motormodell_M->solverInfo, (void *)&motormodell_M->intgData);
  rtsiSetSolverName(&motormodell_M->solverInfo,"ode3");
  rtmSetTPtr(motormodell_M, &motormodell_M->Timing.tArray[0]);
  rtmSetTFinal(motormodell_M, -1);
  motormodell_M->Timing.stepSize0 = 0.2;
  rtmSetFirstInitCond(motormodell_M, 1);

  /* Setup for data logging */
  {
    static RTWLogInfo rt_DataLoggingInfo;
    rt_DataLoggingInfo.loggingInterval = NULL;
    motormodell_M->rtwLogInfo = &rt_DataLoggingInfo;
  }

  /* Setup for data logging */
  {
    rtliSetLogXSignalInfo(motormodell_M->rtwLogInfo, (NULL));
    rtliSetLogXSignalPtrs(motormodell_M->rtwLogInfo, (NULL));
    rtliSetLogT(motormodell_M->rtwLogInfo, "tout");
    rtliSetLogX(motormodell_M->rtwLogInfo, "");
    rtliSetLogXFinal(motormodell_M->rtwLogInfo, "");
    rtliSetLogVarNameModifier(motormodell_M->rtwLogInfo, "rt_");
    rtliSetLogFormat(motormodell_M->rtwLogInfo, 4);
    rtliSetLogMaxRows(motormodell_M->rtwLogInfo, 0);
    rtliSetLogDecimation(motormodell_M->rtwLogInfo, 1);
    rtliSetLogY(motormodell_M->rtwLogInfo, "");
    rtliSetLogYSignalInfo(motormodell_M->rtwLogInfo, (NULL));
    rtliSetLogYSignalPtrs(motormodell_M->rtwLogInfo, (NULL));
  }

  /* block I/O */
  (void) memset(((void *) &motormodell_B), 0,
                sizeof(B_motormodell_T));

  /* states (continuous) */
  {
    (void) memset((void *)&motormodell_X, 0,
                  sizeof(X_motormodell_T));
  }

  /* Periodic continuous states */
  {
    (void) memset((void*) motormodell_PeriodicIndX, 0,
                  1*sizeof(int_T));
    (void) memset((void*) motormodell_PeriodicRngX, 0,
                  2*sizeof(real_T));
  }

  /* states (dwork) */
  (void) memset((void *)&motormodell_DW, 0,
                sizeof(DW_motormodell_T));

  /* external inputs */
  (void)memset(&motormodell_U, 0, sizeof(ExtU_motormodell_T));

  /* external outputs */
  (void) memset((void *)&motormodell_Y, 0,
                sizeof(ExtY_motormodell_T));

  /* Matfile logging */
  rt_StartDataLoggingWithStartTime(motormodell_M->rtwLogInfo, 0.0, rtmGetTFinal
    (motormodell_M), motormodell_M->Timing.stepSize0, (&rtmGetErrorStatus
    (motormodell_M)));

  /* InitializeConditions for Integrator: '<S19>/Integrator' incorporates:
   *  Integrator: '<S18>/Integrator'
   */
  if (rtmIsFirstInitCond(motormodell_M)) {
    motormodell_X.Integrator_CSTATE = 0.0;
    motormodell_X.Integrator_CSTATE_o = 0.0;
  }

  motormodell_DW.Integrator_IWORK = 1;

  /* End of InitializeConditions for Integrator: '<S19>/Integrator' */

  /* InitializeConditions for Integrator: '<S4>/Int1' */
  motormodell_X.Int1_CSTATE = 0.0;

  /* InitializeConditions for Integrator: '<S18>/Integrator' */
  motormodell_DW.Integrator_IWORK_n = 1;

  /* InitializeConditions for Integrator: '<S4>/Int' */
  motormodell_X.Int_CSTATE = 0.0;

  /* InitializeConditions for root-level periodic continuous states */
  {
    int_T rootPeriodicContStateIndices[1] = { 1 };

    real_T rootPeriodicContStateRanges[2] = { -3.1415926535897931,
      3.1415926535897931 };

    (void) memcpy((void*)motormodell_PeriodicIndX, rootPeriodicContStateIndices,
                  1*sizeof(int_T));
    (void) memcpy((void*)motormodell_PeriodicRngX, rootPeriodicContStateRanges,
                  2*sizeof(real_T));
  }

  /* set "at time zero" to false */
  if (rtmIsFirstInitCond(motormodell_M)) {
    rtmSetFirstInitCond(motormodell_M, 0);
  }
}

/* Model terminate function */
void motormodell_terminate(void)
{
  /* (no terminate code required) */
}
